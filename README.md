# BeatSaberBot

Get your Oauth Token here: https://twitchapps.com/tmi/  

Available Commands:

**[Everyone]**  

| Command | Usage                                                                                                                                                         |
|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| !qhelp  | Shows all available commands                                                                                                                                  |
| !add    | Add a song to queue using the BeatSaver ID/Search Text. The Original soundtracks are stored using the values OST1-11 in the order they are displayed in game. |
| !bsr    | Add a song to queue using the BeatSaver ID/Search Text. The Original soundtracks are stored using the values OST1-11 in the order they are displayed in game. |
| !queue  | Shows the current queue                                                                                                                                       |


**[Moderator Only]**  

| Command     | Usage                                                                                                                                |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------|
| !next                 | Removes the current song from queue and displays the next   song.                                                          |
| !clearall             | Clears the queue.                                                                                                          |
| !block                | Adds the song to the banlist (File and array)                                                                              |
| !unblock <id>         | Removes song from the banlist (File and array)                                                                             |
| !close                | Closes the queue.                                                                                                          |
| !open                 | Opens the queue.                                                                                                           |
| !randomize            | Randomizes current Queue & uses value set in "RandomizeLimit"                                                              |
| !clearusers           | Works in conjuction with Randomize, It'll clear users of the already chosen list so they can readd songs to future queues. |
| !mtt <id or username> | This will move all of a users requests to the top of the queue, or will move a single request (by ID) to the top of queue. |

To use the bot, place all files in the same location or in the same folder.  
You'll be using the following files:  

BeatSaberBot.exe
Files located in "Config" folder

Note: Blacklist File is a file that can have songs added to during stream while the shadow banlist is meant more for songs that are banned for another reason, maybe your starter song, ending song, etc. Not displayed to chat.  

Edit BeatSaberBotProperties.xml to have your channel and Oauth token of yourself or a bot you have setup for your channel.  
Here are the options:  

| Property                      | Usage                                                                                                          |
|-------------------------------|----------------------------------------------------------------------------------------------------------------|
| <Username>                    | Your Username                                                                                                  |
| <Oauth>                       | Place your OAuth Token here                                                                                    |
| <Channel>                     | Channel you want to join                                                                                       |
| <ModeratorOnly>               | This allows only Moderators to add to the queue as well as perform any commands.                               |
| <SubscriberOnly>              | This allows Moderators & Subscribers only to add to the queue.                                                 |
| <ViewerRequestLimit>          | *Default: 1*, but this is the number of requests a user can have. Moderators don't have a limit.               |
| <SubscriberLimitOverride>     | *Default: 3*, but this is the number of requests a subscriber can have. Moderators don't have a limit.         |
| <ContinueQueue>               | *Default: false*, If set to true, requests will be kept track of.                                              |
| <Randomize>                   | *Default: false*, This takes the current queue and chooses the randomize limit and creates a new queue.        |
| <RandomizeLimit>              | Limit of songs chosen from current queue to recreate new randomized queue.                                     |
| <BlockUserMultiRandomQueue>   | *Default: false*, If a user is chosen for the random queue, they won't be able to request again until cleared. |
| <OverrideSongInMultipleQueue> | *Default: false*, If set to true, users can request songs already played as long as not currently in queue.    |
| <EnableShadowQueueButton>     | *Default: false*, Displays button for shadow banned list on GUI                                                |
| <BSRCommandEnabled>           | *Default: true*, Allows or disallows the !bsr command which is on by default.                                  |

Example blacklist file is just song ids on new line. With new API changes, the format usually has both numbers with Hyphen, but can be blocked using just the first set.
Similar setup in the shadow_blacklist.txt file.

2021  
2029-1410  
  
  

Incoming Features:
Maybe an ingame menu if I can figure out how to do that...  
  
I stream sometimes, catch me on twitch @ twitch.tv/mr_moogles :)