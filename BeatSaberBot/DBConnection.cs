﻿using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace BeatSaberBot
{
    class DBConnection
    {
        private readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static readonly string connStr = "server=?;user=?;database=?;port=3306;password=?;SslMode=none";
        string downloadURL = "https://bitbucket.org/Mr_Moogles/bsb_gui/downloads/";

        public void runQuery(string query)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand(query, conn);
            try
            {
                logger.Info("Connecting to MySQL...");
                conn.Open();

                var rdr = cmd.ExecuteReader();

                DataTable dataTable = new DataTable();
                dataTable.Load(rdr);

                foreach (DataRow row in dataTable.Rows)
                {
                    logger.Info(row["songKey"].ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                conn.Close();
                logger.Info("Connection Closed.");
            }
        }

        public void runInsert(string query)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand(query, conn);
            try
            {
                logger.Info("Connecting to MySQL...");
                conn.Open();

                var rdr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                conn.Close();
                logger.Info("Connection Closed.");
            }
        }

        public bool checkVersion()
        {
            string query = "SELECT * FROM VERSION;";
            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = new MySqlCommand(query, conn);

            try
            {
                logger.Info("Connecting to MySQL...");
                conn.Open();

                var rdr = cmd.ExecuteReader();

                DataTable dataTable = new DataTable();
                dataTable.Load(rdr);

                Version currVer = null;
                Version newVer = null;
                
                foreach (DataRow row in dataTable.Rows)
                {
                    currVer = new Version("1.7.0");
                    newVer = new Version(row["newVersion"].ToString());
                    downloadURL = downloadURL + row["downloadSuffix"].ToString();
                }

                if (newVer.CompareTo(currVer) > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return false;
            }
            finally
            {
                conn.Close();
                logger.Info("Connection Closed.");
            }
        }

        public string returnQuery(string id, string songName, string uploader, string requestedBy, string channel)
        {
            return "INSERT INTO REQUESTS (songKey, songName, songUploader, songRequestedBy, songStreamerChannel, songRequestDate) " +
                   "VALUES (\"" + id + "\"," +
                   "\"" + songName + "\"," +
                   "\"" + uploader + "\"," +
                   "\"" + requestedBy + "\"," +
                   "\"" + channel + "\"," +
                   "\"" + TimeZoneInfo.ConvertTimeToUtc(DateTime.Now) + "\");";
        }

        public string getDownloadURL()
        {
            return downloadURL;
        }
    }
}
