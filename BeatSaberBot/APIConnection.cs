﻿using System;
using SimpleJSON;
using RestSharp;
using System.Threading.Tasks;
using log4net;

namespace BeatSaberBot
{
    class api
    {
        private const String BEATSAVER = "https://beatsaver.com/";
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<SongInfo> GetBeatSaverSongInfo(Boolean isTextSearch, String queryString, String requestedBy)
        {
            String apiPath;
            String error;
            if (isTextSearch)
            {
                logger.Debug(String.Format("Running Text Search API Call for \"{0}\"", queryString));
                apiPath = "api/songs/search/all/{id}";
                error = "Couldn't find song matching " + "\"" + queryString + "\"";
            }
            else
            {
                logger.Debug(String.Format("Running ID Search API Call for \"{0}\"", queryString));
                apiPath = "api/songs/detail/{id}";
                error = "Song with ID " + queryString + " is Invalid!";
            }

            var client = new RestClient(BEATSAVER);
            var request = new RestRequest(apiPath, Method.GET);
            request.AddUrlSegment("id", queryString);

            IRestResponse response = await client.ExecuteTaskAsync<SongInfo>(request);

            if (!response.StatusDescription.Equals("OK"))
            {
                logger.Error("API Call Error: " + error);
                return null;
            }

            JSONNode node = JSON.Parse(response.Content);

            string songName;
            string beatName;
            string authorName;
            string uploader;
            string bpm;
            string key;
            string downloadUrl;

            logger.Debug("Parsing Response");
            logger.Debug(response.Content);
            if (isTextSearch)
            {
                songName = node["songs"][0]["songName"];
                beatName = node["songs"][0]["name"];
                authorName = node["songs"][0]["authorName"];
                uploader = node["songs"][0]["uploader"];
                bpm = node["songs"][0]["bpm"];
                key = node["songs"][0]["key"];
                downloadUrl = node["songs"][0]["downloadUrl"];
            } else
            {
                songName = node["song"]["songName"];
                beatName = node["song"]["name"];
                authorName = node["song"]["authorName"];
                uploader = node["song"]["uploader"];
                bpm = node["song"]["bpm"];
                key = node["song"]["key"];
                downloadUrl = node["song"]["downloadUrl"];
            }
            logger.Debug("Parsing Response Ended");

            if(String.IsNullOrEmpty(songName) || String.IsNullOrEmpty(uploader) || String.IsNullOrEmpty(key) || String.IsNullOrEmpty(downloadUrl))
            {
                return null;
            }

            return new SongInfo(songName, beatName, authorName, uploader, key, downloadUrl, requestedBy);
        }
    }
}
