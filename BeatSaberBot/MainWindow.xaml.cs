﻿using System;
using System.Windows;
using System.IO;
using System.Xml;
using TwitchLib.Client;
using log4net;
using System.Windows.Documents;
using System.Diagnostics;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace BeatSaberBot
{
    public partial class MainWindow : Window
    {
        private Config config;
        private IRCHandler irc;
        private TwitchClient client;
        private BeatSaberBotFunc bsf;
        private DBConnection db;
        private PathFinder pf = new PathFinder();

        string channel;
        string beatSaberDir;
        bool downloadSongs = false;

        bool allowAudio = false;
        bool isSubOnlyChat = false;
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static List<Tuple<string, string>> songList = new List<Tuple<string, string>>();

        public MainWindow()
        {
            logger.Info("Initializing Beat Saber Bot GUI");
            InitializeComponent();
            //db = new DBConnection();
            //if (db.checkVersion())
            //{
            //    txtUpdStatus.Document.Blocks.Clear();

            //    Hyperlink hlink = new Hyperlink
            //    {
            //        IsEnabled = true
            //    };

            //    hlink.Inlines.Add("Download newer version.");
            //    hlink.NavigateUri = new Uri(db.getDownloadURL());
            //    hlink.RequestNavigate += (sender, args) => Process.Start(args.Uri.ToString());

            //    Paragraph para = new Paragraph();
            //    para.Inlines.Add("Version outdated! ");
            //    para.Inlines.Add(hlink);
            //    para.Inlines.Add(new Run(""));

            //    txtUpdStatus.Document.Blocks.Add(para);
            //}

            beatSaberDir = pf.GetInstallationPath();            
            logger.Debug("Beat Saber Directory Found: " + beatSaberDir);

            if(beatSaberDir != null)
            {
                downloadSongs = true;
            }

            logger.Debug("Allow Downloads to Beat Saber Directory: " + downloadSongs);
            logger.Info("Reading Config Properties");
            config = ReadCredsFromConfig();
            readFromJSON();
            irc = new IRCHandler(config);
            client = irc.GetClient();
            logger.Debug("Config.Channel: " + config.channel);
            logger.Debug("Config.Username: " + config.username);
            logger.Debug("Config.Mod Use Only? " + config.modOnly);
            logger.Debug("Config.Sub Users Only?: " + config.subOnly);
            logger.Debug("Config.Viewer Request Limit: " + config.viewerLimit);
            logger.Debug("Config.Subscriber Request Limit: " + config.subLimit);
            logger.Debug("Config.Allow Queue Randomization: " + config.randomize);
            logger.Debug("Config.Song Limit for Randomized Queue: " + config.randomizeLimit);
            logger.Debug("Config.Prevent User From Request if Chosen: " + config.blockMultiRandomQueue);
            logger.Debug("Config.Allow Repeat Songs in Sequential Queues: " + config.overrideSongInMultiQueue);
            logger.Debug("Config.Enable Shadow Queue Button: " + config.enableShadowQueue);

            channel = config.channel;
            bsf = irc.GetBotFunctions();
            bsf.RetrieveWindowForm(this);
            bsf.setDownloadValues(downloadSongs, beatSaberDir);
            if (config.continueQueue)
            {
                checkStoredPlaylist();
            }
            logger.Info("Reading Blacklisted Songs");
            ReadFromBlacklistFile("song_blacklist");
            ReadFromBlacklistFile("shadow_blacklist");

            // Making List Views Unclickable.
            btn_randomizeQ.IsEnabled = false;
            btn_clear_blockedUsers.IsEnabled = false;
            songInfoLV.IsHitTestVisible = false;
            logger.Info("Finished Initializing");

            if (!config.enableShadowQueue)
            {
                btn_showShadowBanned.Visibility = Visibility.Hidden;
            }

            if (config.randomize)
            {
                btn_randomizeQ.IsEnabled = true;
            }

            if (config.blockMultiRandomQueue)
            {
                btn_clear_blockedUsers.IsEnabled = true;
            }
    }

        private void Btn_Click_Queue(object sender, RoutedEventArgs e)
        {
            bsf.ProcessMessageRecievedAsync("!queue", false);
            bsf.UpdateListViews();
        }

        private void Btn_Click_Next(object sender, RoutedEventArgs e)
        {
            bsf.SetUserRoles(true, false, false, config.username);
            bsf.ProcessMessageRecievedAsync("!next", false);
        }

        private void Btn_Click_Block(object sender, RoutedEventArgs e)
        {
            if (bsf.ReturnSongQueue().Count >= 1)
            {
                bsf.SetUserRoles(true, false, false, config.username);
                bsf.ProcessMessageRecievedAsync("!block " + ((SongInfo)bsf.ReturnSongQueue()[0]).id, false);
                if (allowAudio)
                {
                    bsf.speakAudioFile("Config\\BlockedSong.wav");
                }
                bsf.ProcessMessageRecievedAsync("!next", false);
            }
        }

        private void Btn_Click_ListShadowQueue(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(bsf.ListShadowBannedSongs(), "Shadow Banned Songs", MessageBoxButton.OK);
        }

        private void Btn_Click_ManageQ(object sender, RoutedEventArgs e)
        {
            bsf.SetUserRoles(true, false, false, config.username);
            if (!bsf.AcceptingRequests())
            {
                bsf.ProcessMessageRecievedAsync("!open", false);
                lblQueueStatus.Content = "Queue Status: Open";
            } else
            {
                bsf.ProcessMessageRecievedAsync("!close", false);
                lblQueueStatus.Content = "Queue Status: Closed";
            }
            
        }

        private void Btn_Click_Randomize(object sender, RoutedEventArgs e)
        {
            bsf.SetUserRoles(true, false, false, config.username);
            bsf.ProcessMessageRecievedAsync("!randomize", false);           
        }

        private void Btn_Click_SubOnlyChat(object sender, RoutedEventArgs e)
        {
            if (!isSubOnlyChat)
            {
                client.SendMessage(channel, "/subscribers");
                lblSubChatStatus.Content = "Sub Chat Only: True";
                isSubOnlyChat = true;
            }
            else
            {
                client.SendMessage(channel, "/subscribersoff");
                lblSubChatStatus.Content = "Sub Chat Only: False";
                isSubOnlyChat = false;
            }
        }

        private void Btn_Click_ClearBlocked(object sender, RoutedEventArgs e)
        {
                
            bsf.SetUserRoles(true, false, false, config.username);
            bsf.ProcessMessageRecievedAsync("!clearusers", false);
        }

        private void Btn_Click_ClearQueue(object sender, RoutedEventArgs e)
        {
            if (bsf.ReturnSongQueue().Count >= 1)
            {
                bsf.SetUserRoles(true, false, false, config.username);
                bsf.ProcessMessageRecievedAsync("!clearall", false);
            }
        }

        private void Btn_Click_AllowTTS(object sender, RoutedEventArgs e)
        {
            if (btn_allowTTS.Content.Equals("Allow Text-to-Speech"))
            {
                btn_allowTTS.Content = "Disable Text-to-Speech";
                lblAllowTTS.Content = "Allowing Text To Speech: True";
                bsf.setTTSBool(true);
            } else
            {
                btn_allowTTS.Content = "Allow Text-to-Speech";
                lblAllowTTS.Content = "Allowing Text To Speech: False";
                bsf.setTTSBool(false);
            }
        }

        private void Btn_Click_AllowSoundClip(object sender, RoutedEventArgs e)
        {
            if (btn_allowAudio.Content.Equals("Allow Sounds"))
            {
                btn_allowAudio.Content = "Disable Sounds";
                lblAllowSound.Content = "Allowing Audio Sound Clips: True";
                allowAudio = true;
                bsf.setAudioBool(allowAudio);
            }
            else
            {
                btn_allowAudio.Content = "Allow Sounds";
                lblAllowSound.Content = "Allowing Audio Sound Clips: False";
                allowAudio = false;
                bsf.setAudioBool(allowAudio);
            }
        }

        private void readFromJSON()
        {
            if (!Directory.Exists("Config"))
            {
                Directory.CreateDirectory("Config");
            }

            if (config.continueQueue)
            {
                if (File.Exists("Config\\Playlist.json"))
                {
                    var json = JObject.Parse(File.ReadAllText("Config\\Playlist.json"));

                    if (json["image"] == null)
                    {
                        json.AddFirst(new JProperty("image", "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gAgQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3MA/9sAhAADAwMDAwMEBAQEBQUFBQUHBwYGBwcLCAkICQgLEQsMCwsMCxEPEg8ODxIPGxUTExUbHxoZGh8mIiImMC0wPj5UAQMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlT/wgARCADAAMADASIAAhEBAxEB/8QAHgAAAQQCAwEAAAAAAAAAAAAAAAUHCAkEBgECAwr/2gAIAQEAAAAAqqAAAAAAAAAAFJb1bzAAAAAAH2vfcOqWHLG+Mko74/XnZXk3OIQAAWu2f1Q52gw4ULY67YyZtwc2FyrOrzqAB7X+6OiuZEKPDsZ0vZKaRtSjCKONewABYXc3Xi1YkRvdy4fD3PqJ0fIDVzgAOZ9HqzHStT1bfXbJJxLHY68RUqHj8ABKa+rw9IZwYR39sv3VETfJSX8L5odIAAsgtG25T07VkFlZjNM0bSr7syl0Kj6MvABn/QA6+xr2sYnlA10Y77astw701l+u+vePwBIq+3rt/RKxcWn+YUc21u4helyr3CkuEPABbjPDLU1Lr11KuC0iBrMzfbXV37ayjvqAc/QlJJC1dY9O3hEtsJjtZIfeNQi7Xcxus6kBz9BbyLikgJ+Kvrsc4lvbI1zUSJ1cOnJbH4wOf9GmLseSaTpLw5lfvD5xQs8g5OZlqCdAxwdWSViUr+Omj6q6qiNnX7LiJz4yLWNXiVDGGoKi/efKbwxMFe7Bise3DRSUSGznA2FL0VALf7KEDYADhMykuBuG62pTw4jj8++CGV9Ibs9jjwyDHT1dL0aMEgXa4QGkosbc7WeWxpqt34OQPFLwK3prMY3ssHtrirZaaVV52P74myqWNm8mGg8cRXalQp9crZ9VbrYLv3o1Jy+ePfI65CLnndr47uiybpuIzkHYaJc3IvbXg4Li7JnJDa7RtKahaCvbGntmraOppXkAAG4y3hLggO7vcZ9xlxBYP//EABkBAAIDAQAAAAAAAAAAAAAAAAACAwQFAf/aAAgBAhAAAAAAAAAAShbgu1GtAGVmNq2TJ3g72HDTYnjaYJRbWXdarFKxKyvWZtTCmDszkddebcFAbk8a506SzA69VjAsS3+9OA0mIyX7M4i8k7RHJAADgH//xAAbAQABBQEBAAAAAAAAAAAAAAAAAQIDBAUHBv/aAAgBAxAAAAAAAAAA0Oj87pwTJEAnaOucr4xE/wBh4kGx+h69zzxqOSMM8m8/7bZymVmIZ0Vipt9a4/5f1zQjzqyWNV69ExvJEMmTbffa9YmpXlSWBnpaUVYayUSKntq5ufkFmeWgzSmJoQRHNc4D/8QALxAAAAcAAQIFAwQBBQAAAAAAAQIDBAUGBwgAERASExQgFSExCRYwMhcYIiQzUP/aAAgBAQABCAD/AMCOhpaXP5GDqhXZkTzuFUVUFBTV/iEBD+LAMUltsuiMShnGIZ5m0S3Zw8rHwYsz/UuT9L43WKaB/EvMvpThioeJcIHbLHRP1nfHGyXOjTdxdlaKqLqJJHIKZhKPRSmMPYIam2iwLAlG1rizs1kUAE9L46RmLUYH9r/h/TaIHoXQwzUxHQUW6fvbzpGg8k7I5j4WSwcmcRrV27slJg55gdI1qpR406yrbNaj7uYYS845lc+3LLndLo+rYfA5Ix9w2GOf+XzDAQqs9Ms40uI8RswiI9q9eRFJgIZIqbUTxTJVNE3L3HYVawS9qnVAKBzAX5t0FXSyaCPETJBy3L2gPOcNsfxlJh6yy4xNYeCn/aH5Hz0W4iWcah1bk0Prz0heM2u17M21qh7LYMr1PXzP7/XcHwzIZGnRdiAucUMCeTqd4649YROZzneY1bLoxaOr9/tSNKqUjNKQNxkoyQJN2TnYyRdPqpNB/Bwx49jZJEt9sTcU/bpiTnEQW81QJMyaqqKhFUVll3Kgqry8m3iGKrhZ04O8dKrn4vUyFuWxO1pSvJp+0OmFUqqVXcS4Nih3HxlYmOm2J2Uhaqg4gJ9s4S50LB6VYSH55BQHWmaDC11Co1qGqFdYQsc7UcFIAN+Q2Zf5Tz1/FoQOhtkEDxc3IaOiUolZSkw/l1fUdP3xGCQD1xPyl5QacvNS8KiKTQBHoOhDw79ctaJL2nP0pqG3zYC61JQqxPnw6lvpe5V4vTRyJ1+wwcAaGWenF+3OmPnLunGWuag5NNRs3g24VlwdA5Mk2t23XVJxIxOCsbAbxYo6NO6OURKUCFApZ+zQNWZleTCGk0Rwommg50GuoH8hYyyxsoPZMBAQ+0k1TexrxuppMEat3mdjDfPglliLyQdXh+6nEoUyBz1ufbT7QyyQgAh2GbjlQH/iiVwiYS9XAjs1TmgHhMmg8y8xDSkzHQbfzLSulPFhMVrchj7zGnjbBXciz6sSxZSMe6dn8Q99B/SLBE2hUD19mmok1SKpqFwmqXWVX0PuGXbE4tUtbJ4SmKIgPxi2CspJNGKOO0cmeUODgwcwCNgTImet15Otshb+Es8Sh2D1/IRr5lMR6L5pJNknse7bKcW7QjnExoNNdWTR4VR6P1Oe3ZwaTctK7RNGi70p7RG253rd0sTGqxW78dqPkUlSXyPDEkNVCW+rmUduVx6akUBsX1uX+16hQXv0dro9az+Gr6EvWvjxWqRLltdeYHmmyaDlNUsEV+RZwZwk4+/Yyp/v06QbvW6qDluzax7Urdq5MCbdU47DZH0rs8lL53TuIuZnrKC9josmnR465ZwQky4qOox8owhnhpCIYPDcy59vKRkLQWTfKnsFLNbdEZbcYy/VdOWTnnqUbDvnSuicvX9+r05V7CPbv9vj+n5kTNtDr395Y0DEnCuhbPyAACVnMs3qqiRAOBu4dG6EO4dXQ6gVeWI24q32zVOdTpr9i0jXICq35hZshWXrXRK9jmK3az3RpOXmA3B262B5R4vlZW5qAtVS06Ofcns1TYArG8X6vZK7QFHc7zN3uJodKd1ePOcyhzHN8Q/IdcOCJp4RAAR6xaPCgK0nW0RT9RomJkVPIoKgh2HpJycTdulHA/jpuiaQVFMvIWgXcbXAT9OwW+WXCtDeUnRNtyT/AC3BRZo1jxuXsrgklqVGoVLo0cLWtroIOUjpLMc7o8S9M9abxyYXrcmNFoPIXD7PSo6v2G1FobPyfeaqjqLTFZP4B+Q64agdPBYDzzlqeoSjiKjalYP3JCIPjScO3kEzB0tCqsmpO6X+3v3WXAgiI1jzgkqr0+i2z0hhU1rIIHQoo8PMr6ZtPFxIjWVp/MjN740QLJUfQaZPugZRXUq4SaFKd5rXGFeYs4aDQdsuOoWi9sWOjmUTJ280vKR8e3Erp2KAuVBQ8AARHsGUUFK+aBFVt/RKjEUmrR8HF2ajsrE5K7GKi2kKwRZNl102yfqHWOiZAe75xHthETPgUssi2i2UTGIRLIjVFwKwIKeju07yMRRSQGpcUait7eat1OWx1WrzzmPoW7WW9aG5QobNwqnFoLP5WSluRGvpRcc3QTboJop7phFU2atrtX2lVC2Z1bHsBOOHbl2ICt45Flc7rVnCGiLtA1S24pMWbrh/f5K0ZFDKS3gqummAiexT6TNmoqrW6mraDDLzkdDRkSUSMvDXakpcKhIxiTjcrNl+czVIuPFqmJQmHw6D/lBkiNd0GpSVP03WZyzVqGplWyjOYnMKe0iGty0GvUeDdysnjukv9Ogl5hXnzlDWw1JvbmL/AInLsMMLohh8YibloF0DqNgkI+cSlUHnGTNZWm5hAsH/AEqmcxh8osznH7v6w0enIdUhSkKBQ8XqqJkwR62XDq/qtSViHPHW6W+El7Pnlrkto2e8XCZeUjEMofVsrm1WbWN9iKXKKwbSr43f9fmm9j0uOYs4pkizZbBGtZ6mz0YvZ94eI4g3ytv8OCGI1wlT/e8sHbt9oYliRkHxn/yeSSAdkUzEcER7pFVeLtyKO9Dm77i+oz1veaJl8TTK9H6fm8fynpzGvNnI4Ei/0TY7FoavRjAQBMO26DB0ekTkrKybsH8i7dB4smx3jxBuTCqh+yMqrUEKSRUi+UvgIgACPRkVDuk3Cfg/RcKtj+jExibdJMx1CedMxejoqIJkbku1cbW+pTUC64f2witNn6LLNsbxk0j70rNGJjWxG7RWUZJdP5gzonppabgdc2SKSTndXziay26SNfk/BMh1TlITivw9eqvYq7XM7tqzQKRNSYXFyl5GzxJUoAHX56AAAPiu6TRL9zySo/gFOynn6cW9HDuUVkaHG4wScD9bUPy/xAhDieb5uU9ZwRhUsum5OQhmr6yg6bmKBg5t0ZheK4pY20pl60bnUfbS9cWMUldLv7F248qZCFTTWOomQRImYRKAisyFV82dlLKn7dNnSLn8yf1QqAGj0xMJCibwePSoB5SmUMobubrt1yHxWbuDpjdKfn+5V2dXGvXrkBmeRIZTMyLZAXzVQqqEPZdDi2q7lNvyO2iMRFueybNodqYHYyJ5F2o0I1GjzcLXrG0fzHGrSYXRagueHstwqtNai5n69teU2t8DKKcrtmSAuHTNwzk0QWZCciq52YJEVblKAe6cj16j8yxTFXlQjW513kBodYtouE4FQFDnERBI/XpK9a1qcJjlZCfmofnhjsi4TSXu+X4xusKxsDyMwzjvW5Qq1kKlxED2/ZPUMATblbhotA4o6qwWDqf4WTorLq1ebiHkBLPIx4A9h6zDmnZc5ow1wlz0e2X+ZVkp643+tS8VFEgJvb9OsEChCP6xr+i1DzBFteWG3MxAyaHNXeUgABDnBunT7mZu70olGe2rSrkZJObkNiRo9wCWyr/WpvPbt065hbu6/u45RbS5/vZ9WvtxbC1msmqD+zLyIN5mha/UDM2czqueM6XGREi0qMIe0TzWMNoVTJSp40c3I6cp/wBG9ksLT/oXcLuljrL/AMNBpUzoVqZ12Iis+0XF6csSSkiuQfOPcfDLNR0TM1nshV9A5U6nplccwM11R16u0ssOvYdonOJ1sSYhUPH/xAA+EAACAQMCBAMGBAIIBwEAAAABAgMABBEFEgYhMVETIkEQFCAyYXEwQlKBIzMVJFRicpGUogc0QFCCk7Gy/9oACAEBAAk/AP8AsFjc3TZ6Qxs5/wBtcPapGvdrWQCkZHHVWGCP+lJisoiHvLj0RK0e3V4lGblkBlc9yaitjCB5vFChcDvmuK7DSdSXKXFtDExRyP8ABXGtlLdICRbvHIpk+xIr5kYg+yQafpthAXiaUEGY9hQ3FC3+S+tYyOxz7ASewrSrq4c9AsZrh2eBP1yVq0c2u3pxZ2MBwUH6n/Cg9bbElTpDa2sTSSyMcAAVLd2PCsMxjiig5SXePU1pMKrOPnYl3/8ALdVnEkgBKOo2nP7UCRGx3oeorRtUueH4pc3EtrDvztPStZi0vFusXuzERyxqvcVxvbXmrLJsFray/wAVc/araXHfaamit2uZQglmO1Fz6k0bbiK+KBnlBzCh+hFWNtCFGAEiWnhilf5EyATXH0ayshaz0uRxvUfpRaOVB5fgI0kkjBUVRkkn0AqPGo6kBPcfZuaCpSj8Q33u82OoRfNTQwxxWJWIPgZY1cxy3G/cyqc49nNGbzCjFJYKFls4HjDl3bnsUVoo4XtooQ1va25EElzHnJJC4rSTe31whM73jGZxIpwch64c0vGP7MlcOQIWHWACI/7aS4WCaTewmmMpB+hahkwRkRL3kb5aTUrzU9WAli8JHaKCNzyUAcgRQcS3Fgwlz+DasdM09w9nCRzmkHRqXYpQEDsO1c7ddTZD2B2VI0bgAq6nBFSvLIersck0wGFO0dzR80jEmrCK8ttOgD7ZFyofHI0ihANgUDkF6YratteXIliiHSPlzA+C3S4t3ILRuMjI6GtTENmwCR2zMAh+iiuvgE/gZAu7lRI/ZRzNW6RW9nAkaIBy5DnSKwzhs0ANQtcz2L9pBUMttq1gTDcwMMHK1bkt3epS3ZfQUC8znbFGOZdj0AqLZqutkSOp6pH1SurnPxXMsOo8OTi8hWMnMncVA8H9HWPu8iN6uD+AM+8SlKPWruScXL7sOSdvrgZoVKNK1xR/PQeWX7itGi1GJfkmjkHmrhsWyRRs7ySSAYAom9u4LrZb27fIhWl2RLjHLAwPQUMADkKvobOBpAgklcIMn6muIdNneQZVIrhHY/YA1MXP2qQBux9iB1ltpVKnocqaGDb3sox9zn8CPK2rGKyyOkoqJppJ5RHHGpwWY1G0bpIyPG3VSpwfZtDbuYPagwP0FBudlL/+TWD4V7KG++80yjA8qCkEadz1pFvLVm3eE/PB7irSVZ4gdhdhtUGtftYZQ2GXm2D+1ahHehGAYxflo5YKM1odxrN452RW0JAOT3zXCN1YJqEvjkKAVQY+lAgg8/i5yXM6RJ93OBSgSpapJcfWQjnUrQyQuJIZV5lHHQ1O1xIzs7ysACxY+yYmCLzDHVVFMJIJVyjUMrLBIh/dSKkPvFrqTS2sJ/QRWtWkMpPKJ5MYrRpdVgtWKz3KAmPI6hSKglt9QHW1cYY/YVbT6VpdwN19qhBGE6lRUD3FleXDWuoSSElnfZydqtEj1nT70EuSS0sTruDUx+woAn6itNjg0K/XYNVCbyAa4qseIRq0MQus20kNzbXYYu6hZAuFwObY+JQVjc3H/p81EAuoG2pFKFh4IAxhfagkikUq6noQaiEUKfKg6CsAKpJP0FWN1KdpsdSvIYi6CR/Wjf6prN/bCaW4a4dAryjIAFWCJLba3cSLO2DIYScDnXKWG7jJH6voaGJLi2jkYeg3rmrMXWt6zdp7sengYO7fWotBxDDZKl0MZS6wBlSKiEcqyyQTJ6rLCdrUwVILeSTPT5VJFaFBd28kkq2k2QGgIOA1dPiizczu0Nkf0AcnqSTHgqoTPlo+YU2JIsbx96PtyZjaS7cdclTXDqW1nq91LNcXc6eeVg5AIq4SWNGIBRgwBHpyq4jt9TcLbS2XpefYd6sk0PTYJEnPvUiKZz1AWtJN/otpbxAalb42QHb8rmrOS9i0aYC+gjGSsOMZqW4v7+RP4VjHC+/xD+U5FQPa3WqXtxdC2PWNJX3LVykms6rFsKIcmFO5o5LEk/c/H3lqMN2ryMnpS7HB5+maNc6NAbB87GoIJ7mwjNu0L+RcOcl/LUuy11+Y3FldlyYUkkO8oGNakbS/0u497sZlwV3ketcQT6zPGmyC0V/DghUdimK0+K1iY+fBLFvuxqNJEYYZWAIP7GtEso7hjlpBEtWL6rxRcgIBEMpAG9a1qW+17iC6b3qNjkRZXNXEm7HYUfFi9T6j4uu+arVJriG2E7Fn2ALUZiZgwdc5AIOOtDa+OTipvFIGCe/twVZjjBpdpxyIqE7wd1rdpykhYdCCKv7HiTh6A4hD3CJc4qz1HS5n9RbvLHn/AB1fSXMrgn+WQB7L6K0hLhQXYKCT6ZrVf6O4kSMMn50nqE2dzpcQjjTZiOVxyLg06jPTJpsiQEBeuc1/LLEr8Fz7hHczoszyeQgH71j3W0hVFIPXA61cT2k6qFd4W2ll7Gl2xxLgCumQM10I9auEA+pqZljkybiVOqqPTP1ouY0HVjkmgPE2nZnpmvcdL097oxS6lEhUxpI2FNa7e8TXDxCXfLJugYdc4atHsDpWgblm2Q5Q7Bk4qz4c4d0y1uRGouoCJbhc08YkWLdKV5LkDnj6VPInCvDVyHuZkOFuJ09KGEjUKo+gqBItQjQ+6XijDo1eKk9pIVRj0ZfQipGcgYGT8E1vFNHGZiZm2jCULfSeMv8Ah9NYWd29qQg1CK5nEKB+7p1V6naadXli8VupCHA9uDjoKbbHGOYpGEL/APLWpJG1e7VAsSn9z7ZfCeeBlRv0vjy1YzW+sWVn7tpF8oOy6QnaMGoQ02pQvLdo35i7VwNdvawXRnv5LAOTMVIYA1Yz2eva5DGs0DAh7OI9S1KPECB7qY/NLIeZJq6SKGBCQSfmPYVpk1jatOy2wkBUyR+j1APfdN5TEDrDWtRvMVVzbDBTY3wXk9pMARvicocH7VJqc+q3CRNpyQASJJP4nmEwOW+UnaRURglwZ3U9R4nP2GjWCvLKEdcUAABgD4F3mTkaeOC65NaXRTc0Lip0uG4ZwsF6qBFaELmp7NtN06Y20VpJt3O2dpepFu+ItUJeWTqIg3PYlW9xqes9I7GBCf8AMipja6ZE4ktdGQ8j234qBIIIUCxogwABSgxy6bMpFBy1tqdz71OT1RZPIB8NlFdXt1JmyaRciJB7JYXtmcm3CAggem745AZGbbgUBlUJ+5qPY4bGPpWiJf8ACGuTql3Oi+eFCMGrq8NvLNHcXdsr7kaOY5bkK1WEsYRmDY28HtWmNHp0kLRW8sqdWPQr7OgFTqglt3it4yecjkcgKXaJ53kx23HPwKWaWVEAAyTuOKj2PDZgv9381dM5+C4YRgfyx09rlWx6VHmY5LMaOMgjNAuh6ueoqMPHfWckWCM4JFKZ30C/ngKSjJMTvgVw5ZrOzbtxz1oQwQoMBEAUVJn7UCqGnljaCMi3C+h7moWQwSkwt6PGT5T7VLMxAAHUk1iG2TE9pYnq/YtRGAoAx2FRjwhnec0QDnmPwOvahgV3qylubHW4IZXSFSWBxnOBUrx2wgEzAoQ6qe61q12hQkFWt2FaVJqt3N5YzKTAoNXdi19cgSG3hlRvBB/L5etN5cUsaXmjnG/oZENavpsq3MYM1lvb3mJjMY1AA5EEYPstHXSNOlWa4mdcKSnNRShI0UKiDoAKQuw6LQwSBkVPLGYD8inCv96FciBWwyg9HUnlXJsDPtOWNHJ9swt+JdJBIX+0p+g1bHRNajbwbi3nGElYdqg0q1vViaW2mTG92xyAxQlR/wArKCDV9xBA+0e7vDvC5/vVxDfkYwRKzZrWJ3t3+eMMQGqQ+Ev5f3zWkQ6raROC9rKSFf8AyrhcaBa2bAIiJiOTPZq1e00+P08aQKTXFFhNcnpH4oy1TxwQDrK7ALVzFcxHo8bBhW8S7Ccj0FMxI9ac1KwQDmvep0ghQZaSRtqgVrFnqL2xIlEEgYpXU0KWra5mtzMIgsChjk1BqdruPztGMCpLZA6B4dQiYK9cfR6zFAf4Nld3ICJUvDX9XbMX8bpWvcNiJRgJmOtc0Kwv3+S8gmAcVxXoOqWiAmPNziVqULcWkzRSgfqX2aLbTvEhFtcLiPZWoz3TSybjGWOwDPQLVhcaPfWMcatLFHGnjHnuJdGyMcsVr909nCAFUOQxx3atfvYw35WlZlrXxnuYgTWs27/e3WtStf8ATrWtwoP7sCitev5LXxFMqQuUyuedXOq6bZNaRRFNQMbzMyDzmQREpzNazb/6da19B9oAK4il/YYrWbm7hznw3clc1we/EX9XaNB4s0awO4wJP4XUj0Bo6xpGjyTYVhJIYoV+vQVx/ovERv1US29ndSTTwvsDlpAyIFWr6O0WdsGaVgqoO5JrVYtSiCBhcxEFDn7VK6/Y4rVb2L/BO6//AA1I8sjnLu5LMT3JP4SRPe3Udy8ayMVUi2hedx99qHA9TXBmkaqdatElia5ti81sJFyAc4IfnzHoag93lMjFotpTae2D0+HUGgiRUN1GXAjbJ2rlfWprRrOcYcJHg+xPG0+PUYTeRFXIMA5t8nOrqXRha6XqDPFbaVcD3m8wPdlcy9F+D//EACsRAAICAQMBCAICAwAAAAAAAAECAxEABBIhMRATICJBUWGBBUIUIzJAcf/aAAgBAgEBPwD/AEJJEiXcx4xvySxvtaFvvrkeojlj3qfrIdVvdlew18LWWKz+UzkiKMkD9jwMQzO3nYCvRR4tfK0U8JqwvNZqJjPIXIq80zAd6Gvbss/WQASwqW+hiKE6ADGB2nbwaNZoe9TWFHuyDfgAs4RtOarRjWRijTDph/F63dWz7vNm0mBPMxPnORJsRUHoM2n2xicRVJDEcgUD4Iltv+DJav5xGIOd5fpkGm2vISOrnnECEsFolevxlqXVBQsgXkyNuIPBBIOAlTQW8VtwvthsvjoSbrAKxeuO5Ln361mgUQ6uaaRxsaPaFyMIZl3GlLC/gZ+WGj8jQkFz/lWd0jNuI5HQ+AXfGLKRw2DaRxjDYl4ESUk/vWSh0U0PN6A4hcqCwo+owOWYjbn4/TRSRM0ig80M10EULju2HPVb6dpBWs6nFAQWxx5C5+MS7oGiRxkUMxlZpiSVPF5NPHCOTyeg9ScUrCpeVgGbk4moHdhgSFYjCS3JN9rMWr4GBiMLFqv07Kqiceytnk4JO41BOpJNcp6jnIIBqLmksgk7VOa1v61RerMKGKKA7KJwqR2jCS5xQR1zVTPDNTKrL+tjO/1TClhrIDvlMkkgLDgDItRFKLU3TbcQAnLA7CgOd2c2HAoA7NVo4dUATww6HNNpmgjKs92ceGM9VU/WJHHGKRQo+BgJGWcs5Z983H3yzm4++WfD/8QALhEAAgEDAgUDAwQDAQAAAAAAAQIDAAQRBRIGEyExQRBRYQcgIhQyUnFAQlNi/9oACAEDAQE/AP8AA0zS73WLtba0j3uQSSThVUd2YnsBVv8ASe5v9PFza61bSP1wI1LISO43Vqmj32j3RtrpNreCOzD3FPDtAI6jHU+nKVcb2x8CmCAdFPXyftyK+m2i2+s8N67CJBHNcssJkAyVXGa4c0SLhnRorITGQRlneQjGS3U19REimsYpwAHF1hD8MDkVKSshAokk9SaiMYlQyAlAw3AdyK1w2tzpUcsAXaGXbjwO2PsdtilsZwKSXmrlRXCXGl3wZfyNt328+BKn9diPkVL9UOHLuEO9+QCM8vYQa4h4ibW5Y2CmO3iyY0Pck/7GmbcxPvSxu/7VJraR3GKLuEMYY7CQSPs1GYxQgDu7AVY8zb/561dxJLHg0bEJ+W/5FQR/qUjfOE2jFR6dJyOcLd+V/wBNp2nHzV1epaI/Kj3bASQPOKjY36JIu0IQCD/dMuGIz2NEY9dTKrbZIz+QxVpcIEChqZ91Tj8CfjpVmsKW6o7sVQYGK0XizQYeE10+dTHOjvvGCRICMA1dM4hmaFQX2sUU+/gVwu+rhZ47oSLEoGwMMYPsKDEDH2SbNuHGQfGM1PYJIBJBhSR28Gm5sTgOCtRNz7hYz2800s+nEAgG33Ht3GahlinAZWyp8imwGODkVgAA5rg/RNOvbGae7ijlJk2rluwFcWaRp+mXSfpJV/L98O7JQ+qvFcBh32tgj2Io4RfAAqeRruXlwoDt7satLQW65bBc9zV50h3FQwVgSD7VzITEvJ27WHcUkbOenbyabMhCoOg7UDNHujV2HkgHocU7vIxZ2LMe5JyfQjIqGBbctjJMjEsaaJWPWo4IoixRQCxyfQyCTcqEHHfzUOyOTYqhVPikto7u2H6dlRx0cOwGRUzmAmJSOnQsKgA3lj2Ao+hdVGSRSTRucKfVhuUj3FIkdvGFGAAKnkRmBU9qgUSRAgkHHXFcuEd5M1J0QIinBq5s57UqJFwWQMP6NTuyRkiirsCeppc+KS6kU/l1FC8TPY0LmMjPWpZmlb48CgCxAAq2up7X8WUlamu1kcYQ1HO+em5almlmbdI7OfcnNMquMEVsXp8VtXGMCuXH/EVyo/4Cti+1cqP+AoIo7Afb/9k="));

                        FileInfo fi = new FileInfo("Config\\Playlist.json");
                        using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                        {
                            txtWriter.Write(json);
                        }
                    }

                    using (StreamReader r = new StreamReader("Config\\Playlist.json"))
                    {
                        var result = JObject.Parse(File.ReadAllText("Config\\Playlist.json"));
                        var songs = result["songs"].Children().ToList();

                        for (int i = 0; i < songs.Count(); i++)
                        {
                            JToken token = songs[i];
                            songList.Add(new Tuple<string, string>
                                ((string)token["id"], (string)token["requestedBy"]));
                        }
                    }
                }
                else
                    using (StreamWriter file = File.CreateText("Config\\Playlist.json"))
                    {
                        JTokenWriter writer = new JTokenWriter();
                        writer.WriteStartObject();
                        writer.WritePropertyName("image");
                        writer.WriteValue("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gAgQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3MA/9sAhAADAwMDAwMEBAQEBQUFBQUHBwYGBwcLCAkICQgLEQsMCwsMCxEPEg8ODxIPGxUTExUbHxoZGh8mIiImMC0wPj5UAQMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlT/wgARCADAAMADASIAAhEBAxEB/8QAHgAAAQQCAwEAAAAAAAAAAAAAAAUHCAkEBgECAwr/2gAIAQEAAAAAqqAAAAAAAAAAFJb1bzAAAAAAH2vfcOqWHLG+Mko74/XnZXk3OIQAAWu2f1Q52gw4ULY67YyZtwc2FyrOrzqAB7X+6OiuZEKPDsZ0vZKaRtSjCKONewABYXc3Xi1YkRvdy4fD3PqJ0fIDVzgAOZ9HqzHStT1bfXbJJxLHY68RUqHj8ABKa+rw9IZwYR39sv3VETfJSX8L5odIAAsgtG25T07VkFlZjNM0bSr7syl0Kj6MvABn/QA6+xr2sYnlA10Y77astw701l+u+vePwBIq+3rt/RKxcWn+YUc21u4helyr3CkuEPABbjPDLU1Lr11KuC0iBrMzfbXV37ayjvqAc/QlJJC1dY9O3hEtsJjtZIfeNQi7Xcxus6kBz9BbyLikgJ+Kvrsc4lvbI1zUSJ1cOnJbH4wOf9GmLseSaTpLw5lfvD5xQs8g5OZlqCdAxwdWSViUr+Omj6q6qiNnX7LiJz4yLWNXiVDGGoKi/efKbwxMFe7Bise3DRSUSGznA2FL0VALf7KEDYADhMykuBuG62pTw4jj8++CGV9Ibs9jjwyDHT1dL0aMEgXa4QGkosbc7WeWxpqt34OQPFLwK3prMY3ssHtrirZaaVV52P74myqWNm8mGg8cRXalQp9crZ9VbrYLv3o1Jy+ePfI65CLnndr47uiybpuIzkHYaJc3IvbXg4Li7JnJDa7RtKahaCvbGntmraOppXkAAG4y3hLggO7vcZ9xlxBYP//EABkBAAIDAQAAAAAAAAAAAAAAAAACAwQFAf/aAAgBAhAAAAAAAAAAShbgu1GtAGVmNq2TJ3g72HDTYnjaYJRbWXdarFKxKyvWZtTCmDszkddebcFAbk8a506SzA69VjAsS3+9OA0mIyX7M4i8k7RHJAADgH//xAAbAQABBQEBAAAAAAAAAAAAAAAAAQIDBAUHBv/aAAgBAxAAAAAAAAAA0Oj87pwTJEAnaOucr4xE/wBh4kGx+h69zzxqOSMM8m8/7bZymVmIZ0Vipt9a4/5f1zQjzqyWNV69ExvJEMmTbffa9YmpXlSWBnpaUVYayUSKntq5ufkFmeWgzSmJoQRHNc4D/8QALxAAAAcAAQIFAwQBBQAAAAAAAQIDBAUGBwgAERASExQgFSExCRYwMhcYIiQzUP/aAAgBAQABCAD/AMCOhpaXP5GDqhXZkTzuFUVUFBTV/iEBD+LAMUltsuiMShnGIZ5m0S3Zw8rHwYsz/UuT9L43WKaB/EvMvpThioeJcIHbLHRP1nfHGyXOjTdxdlaKqLqJJHIKZhKPRSmMPYIam2iwLAlG1rizs1kUAE9L46RmLUYH9r/h/TaIHoXQwzUxHQUW6fvbzpGg8k7I5j4WSwcmcRrV27slJg55gdI1qpR406yrbNaj7uYYS845lc+3LLndLo+rYfA5Ix9w2GOf+XzDAQqs9Ms40uI8RswiI9q9eRFJgIZIqbUTxTJVNE3L3HYVawS9qnVAKBzAX5t0FXSyaCPETJBy3L2gPOcNsfxlJh6yy4xNYeCn/aH5Hz0W4iWcah1bk0Prz0heM2u17M21qh7LYMr1PXzP7/XcHwzIZGnRdiAucUMCeTqd4649YROZzneY1bLoxaOr9/tSNKqUjNKQNxkoyQJN2TnYyRdPqpNB/Bwx49jZJEt9sTcU/bpiTnEQW81QJMyaqqKhFUVll3Kgqry8m3iGKrhZ04O8dKrn4vUyFuWxO1pSvJp+0OmFUqqVXcS4Nih3HxlYmOm2J2Uhaqg4gJ9s4S50LB6VYSH55BQHWmaDC11Co1qGqFdYQsc7UcFIAN+Q2Zf5Tz1/FoQOhtkEDxc3IaOiUolZSkw/l1fUdP3xGCQD1xPyl5QacvNS8KiKTQBHoOhDw79ctaJL2nP0pqG3zYC61JQqxPnw6lvpe5V4vTRyJ1+wwcAaGWenF+3OmPnLunGWuag5NNRs3g24VlwdA5Mk2t23XVJxIxOCsbAbxYo6NO6OURKUCFApZ+zQNWZleTCGk0Rwommg50GuoH8hYyyxsoPZMBAQ+0k1TexrxuppMEat3mdjDfPglliLyQdXh+6nEoUyBz1ufbT7QyyQgAh2GbjlQH/iiVwiYS9XAjs1TmgHhMmg8y8xDSkzHQbfzLSulPFhMVrchj7zGnjbBXciz6sSxZSMe6dn8Q99B/SLBE2hUD19mmok1SKpqFwmqXWVX0PuGXbE4tUtbJ4SmKIgPxi2CspJNGKOO0cmeUODgwcwCNgTImet15Otshb+Es8Sh2D1/IRr5lMR6L5pJNknse7bKcW7QjnExoNNdWTR4VR6P1Oe3ZwaTctK7RNGi70p7RG253rd0sTGqxW78dqPkUlSXyPDEkNVCW+rmUduVx6akUBsX1uX+16hQXv0dro9az+Gr6EvWvjxWqRLltdeYHmmyaDlNUsEV+RZwZwk4+/Yyp/v06QbvW6qDluzax7Urdq5MCbdU47DZH0rs8lL53TuIuZnrKC9josmnR465ZwQky4qOox8owhnhpCIYPDcy59vKRkLQWTfKnsFLNbdEZbcYy/VdOWTnnqUbDvnSuicvX9+r05V7CPbv9vj+n5kTNtDr395Y0DEnCuhbPyAACVnMs3qqiRAOBu4dG6EO4dXQ6gVeWI24q32zVOdTpr9i0jXICq35hZshWXrXRK9jmK3az3RpOXmA3B262B5R4vlZW5qAtVS06Ofcns1TYArG8X6vZK7QFHc7zN3uJodKd1ePOcyhzHN8Q/IdcOCJp4RAAR6xaPCgK0nW0RT9RomJkVPIoKgh2HpJycTdulHA/jpuiaQVFMvIWgXcbXAT9OwW+WXCtDeUnRNtyT/AC3BRZo1jxuXsrgklqVGoVLo0cLWtroIOUjpLMc7o8S9M9abxyYXrcmNFoPIXD7PSo6v2G1FobPyfeaqjqLTFZP4B+Q64agdPBYDzzlqeoSjiKjalYP3JCIPjScO3kEzB0tCqsmpO6X+3v3WXAgiI1jzgkqr0+i2z0hhU1rIIHQoo8PMr6ZtPFxIjWVp/MjN740QLJUfQaZPugZRXUq4SaFKd5rXGFeYs4aDQdsuOoWi9sWOjmUTJ280vKR8e3Erp2KAuVBQ8AARHsGUUFK+aBFVt/RKjEUmrR8HF2ajsrE5K7GKi2kKwRZNl102yfqHWOiZAe75xHthETPgUssi2i2UTGIRLIjVFwKwIKeju07yMRRSQGpcUait7eat1OWx1WrzzmPoW7WW9aG5QobNwqnFoLP5WSluRGvpRcc3QTboJop7phFU2atrtX2lVC2Z1bHsBOOHbl2ICt45Flc7rVnCGiLtA1S24pMWbrh/f5K0ZFDKS3gqummAiexT6TNmoqrW6mraDDLzkdDRkSUSMvDXakpcKhIxiTjcrNl+czVIuPFqmJQmHw6D/lBkiNd0GpSVP03WZyzVqGplWyjOYnMKe0iGty0GvUeDdysnjukv9Ogl5hXnzlDWw1JvbmL/AInLsMMLohh8YibloF0DqNgkI+cSlUHnGTNZWm5hAsH/AEqmcxh8osznH7v6w0enIdUhSkKBQ8XqqJkwR62XDq/qtSViHPHW6W+El7Pnlrkto2e8XCZeUjEMofVsrm1WbWN9iKXKKwbSr43f9fmm9j0uOYs4pkizZbBGtZ6mz0YvZ94eI4g3ytv8OCGI1wlT/e8sHbt9oYliRkHxn/yeSSAdkUzEcER7pFVeLtyKO9Dm77i+oz1veaJl8TTK9H6fm8fynpzGvNnI4Ei/0TY7FoavRjAQBMO26DB0ekTkrKybsH8i7dB4smx3jxBuTCqh+yMqrUEKSRUi+UvgIgACPRkVDuk3Cfg/RcKtj+jExibdJMx1CedMxejoqIJkbku1cbW+pTUC64f2witNn6LLNsbxk0j70rNGJjWxG7RWUZJdP5gzonppabgdc2SKSTndXziay26SNfk/BMh1TlITivw9eqvYq7XM7tqzQKRNSYXFyl5GzxJUoAHX56AAAPiu6TRL9zySo/gFOynn6cW9HDuUVkaHG4wScD9bUPy/xAhDieb5uU9ZwRhUsum5OQhmr6yg6bmKBg5t0ZheK4pY20pl60bnUfbS9cWMUldLv7F248qZCFTTWOomQRImYRKAisyFV82dlLKn7dNnSLn8yf1QqAGj0xMJCibwePSoB5SmUMobubrt1yHxWbuDpjdKfn+5V2dXGvXrkBmeRIZTMyLZAXzVQqqEPZdDi2q7lNvyO2iMRFueybNodqYHYyJ5F2o0I1GjzcLXrG0fzHGrSYXRagueHstwqtNai5n69teU2t8DKKcrtmSAuHTNwzk0QWZCciq52YJEVblKAe6cj16j8yxTFXlQjW513kBodYtouE4FQFDnERBI/XpK9a1qcJjlZCfmofnhjsi4TSXu+X4xusKxsDyMwzjvW5Qq1kKlxED2/ZPUMATblbhotA4o6qwWDqf4WTorLq1ebiHkBLPIx4A9h6zDmnZc5ow1wlz0e2X+ZVkp643+tS8VFEgJvb9OsEChCP6xr+i1DzBFteWG3MxAyaHNXeUgABDnBunT7mZu70olGe2rSrkZJObkNiRo9wCWyr/WpvPbt065hbu6/u45RbS5/vZ9WvtxbC1msmqD+zLyIN5mha/UDM2czqueM6XGREi0qMIe0TzWMNoVTJSp40c3I6cp/wBG9ksLT/oXcLuljrL/AMNBpUzoVqZ12Iis+0XF6csSSkiuQfOPcfDLNR0TM1nshV9A5U6nplccwM11R16u0ssOvYdonOJ1sSYhUPH/xAA+EAACAQMCBAMGBAIIBwEAAAABAgMABBEFEgYhMVETIkEQFCAyYXEwQlKBIzMVJFRicpGUogc0QFCCk7Gy/9oACAEBAAk/AP8AsFjc3TZ6Qxs5/wBtcPapGvdrWQCkZHHVWGCP+lJisoiHvLj0RK0e3V4lGblkBlc9yaitjCB5vFChcDvmuK7DSdSXKXFtDExRyP8ABXGtlLdICRbvHIpk+xIr5kYg+yQafpthAXiaUEGY9hQ3FC3+S+tYyOxz7ASewrSrq4c9AsZrh2eBP1yVq0c2u3pxZ2MBwUH6n/Cg9bbElTpDa2sTSSyMcAAVLd2PCsMxjiig5SXePU1pMKrOPnYl3/8ALdVnEkgBKOo2nP7UCRGx3oeorRtUueH4pc3EtrDvztPStZi0vFusXuzERyxqvcVxvbXmrLJsFray/wAVc/araXHfaamit2uZQglmO1Fz6k0bbiK+KBnlBzCh+hFWNtCFGAEiWnhilf5EyATXH0ayshaz0uRxvUfpRaOVB5fgI0kkjBUVRkkn0AqPGo6kBPcfZuaCpSj8Q33u82OoRfNTQwxxWJWIPgZY1cxy3G/cyqc49nNGbzCjFJYKFls4HjDl3bnsUVoo4XtooQ1va25EElzHnJJC4rSTe31whM73jGZxIpwch64c0vGP7MlcOQIWHWACI/7aS4WCaTewmmMpB+hahkwRkRL3kb5aTUrzU9WAli8JHaKCNzyUAcgRQcS3Fgwlz+DasdM09w9nCRzmkHRqXYpQEDsO1c7ddTZD2B2VI0bgAq6nBFSvLIersck0wGFO0dzR80jEmrCK8ttOgD7ZFyofHI0ihANgUDkF6YratteXIliiHSPlzA+C3S4t3ILRuMjI6GtTENmwCR2zMAh+iiuvgE/gZAu7lRI/ZRzNW6RW9nAkaIBy5DnSKwzhs0ANQtcz2L9pBUMttq1gTDcwMMHK1bkt3epS3ZfQUC8znbFGOZdj0AqLZqutkSOp6pH1SurnPxXMsOo8OTi8hWMnMncVA8H9HWPu8iN6uD+AM+8SlKPWruScXL7sOSdvrgZoVKNK1xR/PQeWX7itGi1GJfkmjkHmrhsWyRRs7ySSAYAom9u4LrZb27fIhWl2RLjHLAwPQUMADkKvobOBpAgklcIMn6muIdNneQZVIrhHY/YA1MXP2qQBux9iB1ltpVKnocqaGDb3sox9zn8CPK2rGKyyOkoqJppJ5RHHGpwWY1G0bpIyPG3VSpwfZtDbuYPagwP0FBudlL/+TWD4V7KG++80yjA8qCkEadz1pFvLVm3eE/PB7irSVZ4gdhdhtUGtftYZQ2GXm2D+1ahHehGAYxflo5YKM1odxrN452RW0JAOT3zXCN1YJqEvjkKAVQY+lAgg8/i5yXM6RJ93OBSgSpapJcfWQjnUrQyQuJIZV5lHHQ1O1xIzs7ysACxY+yYmCLzDHVVFMJIJVyjUMrLBIh/dSKkPvFrqTS2sJ/QRWtWkMpPKJ5MYrRpdVgtWKz3KAmPI6hSKglt9QHW1cYY/YVbT6VpdwN19qhBGE6lRUD3FleXDWuoSSElnfZydqtEj1nT70EuSS0sTruDUx+woAn6itNjg0K/XYNVCbyAa4qseIRq0MQus20kNzbXYYu6hZAuFwObY+JQVjc3H/p81EAuoG2pFKFh4IAxhfagkikUq6noQaiEUKfKg6CsAKpJP0FWN1KdpsdSvIYi6CR/Wjf6prN/bCaW4a4dAryjIAFWCJLba3cSLO2DIYScDnXKWG7jJH6voaGJLi2jkYeg3rmrMXWt6zdp7sengYO7fWotBxDDZKl0MZS6wBlSKiEcqyyQTJ6rLCdrUwVILeSTPT5VJFaFBd28kkq2k2QGgIOA1dPiizczu0Nkf0AcnqSTHgqoTPlo+YU2JIsbx96PtyZjaS7cdclTXDqW1nq91LNcXc6eeVg5AIq4SWNGIBRgwBHpyq4jt9TcLbS2XpefYd6sk0PTYJEnPvUiKZz1AWtJN/otpbxAalb42QHb8rmrOS9i0aYC+gjGSsOMZqW4v7+RP4VjHC+/xD+U5FQPa3WqXtxdC2PWNJX3LVykms6rFsKIcmFO5o5LEk/c/H3lqMN2ryMnpS7HB5+maNc6NAbB87GoIJ7mwjNu0L+RcOcl/LUuy11+Y3FldlyYUkkO8oGNakbS/0u497sZlwV3ketcQT6zPGmyC0V/DghUdimK0+K1iY+fBLFvuxqNJEYYZWAIP7GtEso7hjlpBEtWL6rxRcgIBEMpAG9a1qW+17iC6b3qNjkRZXNXEm7HYUfFi9T6j4uu+arVJriG2E7Fn2ALUZiZgwdc5AIOOtDa+OTipvFIGCe/twVZjjBpdpxyIqE7wd1rdpykhYdCCKv7HiTh6A4hD3CJc4qz1HS5n9RbvLHn/AB1fSXMrgn+WQB7L6K0hLhQXYKCT6ZrVf6O4kSMMn50nqE2dzpcQjjTZiOVxyLg06jPTJpsiQEBeuc1/LLEr8Fz7hHczoszyeQgH71j3W0hVFIPXA61cT2k6qFd4W2ll7Gl2xxLgCumQM10I9auEA+pqZljkybiVOqqPTP1ouY0HVjkmgPE2nZnpmvcdL097oxS6lEhUxpI2FNa7e8TXDxCXfLJugYdc4atHsDpWgblm2Q5Q7Bk4qz4c4d0y1uRGouoCJbhc08YkWLdKV5LkDnj6VPInCvDVyHuZkOFuJ09KGEjUKo+gqBItQjQ+6XijDo1eKk9pIVRj0ZfQipGcgYGT8E1vFNHGZiZm2jCULfSeMv8Ah9NYWd29qQg1CK5nEKB+7p1V6naadXli8VupCHA9uDjoKbbHGOYpGEL/APLWpJG1e7VAsSn9z7ZfCeeBlRv0vjy1YzW+sWVn7tpF8oOy6QnaMGoQ02pQvLdo35i7VwNdvawXRnv5LAOTMVIYA1Yz2eva5DGs0DAh7OI9S1KPECB7qY/NLIeZJq6SKGBCQSfmPYVpk1jatOy2wkBUyR+j1APfdN5TEDrDWtRvMVVzbDBTY3wXk9pMARvicocH7VJqc+q3CRNpyQASJJP4nmEwOW+UnaRURglwZ3U9R4nP2GjWCvLKEdcUAABgD4F3mTkaeOC65NaXRTc0Lip0uG4ZwsF6qBFaELmp7NtN06Y20VpJt3O2dpepFu+ItUJeWTqIg3PYlW9xqes9I7GBCf8AMipja6ZE4ktdGQ8j234qBIIIUCxogwABSgxy6bMpFBy1tqdz71OT1RZPIB8NlFdXt1JmyaRciJB7JYXtmcm3CAggem745AZGbbgUBlUJ+5qPY4bGPpWiJf8ACGuTql3Oi+eFCMGrq8NvLNHcXdsr7kaOY5bkK1WEsYRmDY28HtWmNHp0kLRW8sqdWPQr7OgFTqglt3it4yecjkcgKXaJ53kx23HPwKWaWVEAAyTuOKj2PDZgv9381dM5+C4YRgfyx09rlWx6VHmY5LMaOMgjNAuh6ueoqMPHfWckWCM4JFKZ30C/ngKSjJMTvgVw5ZrOzbtxz1oQwQoMBEAUVJn7UCqGnljaCMi3C+h7moWQwSkwt6PGT5T7VLMxAAHUk1iG2TE9pYnq/YtRGAoAx2FRjwhnec0QDnmPwOvahgV3qylubHW4IZXSFSWBxnOBUrx2wgEzAoQ6qe61q12hQkFWt2FaVJqt3N5YzKTAoNXdi19cgSG3hlRvBB/L5etN5cUsaXmjnG/oZENavpsq3MYM1lvb3mJjMY1AA5EEYPstHXSNOlWa4mdcKSnNRShI0UKiDoAKQuw6LQwSBkVPLGYD8inCv96FciBWwyg9HUnlXJsDPtOWNHJ9swt+JdJBIX+0p+g1bHRNajbwbi3nGElYdqg0q1vViaW2mTG92xyAxQlR/wArKCDV9xBA+0e7vDvC5/vVxDfkYwRKzZrWJ3t3+eMMQGqQ+Ev5f3zWkQ6raROC9rKSFf8AyrhcaBa2bAIiJiOTPZq1e00+P08aQKTXFFhNcnpH4oy1TxwQDrK7ALVzFcxHo8bBhW8S7Ccj0FMxI9ac1KwQDmvep0ghQZaSRtqgVrFnqL2xIlEEgYpXU0KWra5mtzMIgsChjk1BqdruPztGMCpLZA6B4dQiYK9cfR6zFAf4Nld3ICJUvDX9XbMX8bpWvcNiJRgJmOtc0Kwv3+S8gmAcVxXoOqWiAmPNziVqULcWkzRSgfqX2aLbTvEhFtcLiPZWoz3TSybjGWOwDPQLVhcaPfWMcatLFHGnjHnuJdGyMcsVr909nCAFUOQxx3atfvYw35WlZlrXxnuYgTWs27/e3WtStf8ATrWtwoP7sCitev5LXxFMqQuUyuedXOq6bZNaRRFNQMbzMyDzmQREpzNazb/6da19B9oAK4il/YYrWbm7hznw3clc1we/EX9XaNB4s0awO4wJP4XUj0Bo6xpGjyTYVhJIYoV+vQVx/ovERv1US29ndSTTwvsDlpAyIFWr6O0WdsGaVgqoO5JrVYtSiCBhcxEFDn7VK6/Y4rVb2L/BO6//AA1I8sjnLu5LMT3JP4SRPe3Udy8ayMVUi2hedx99qHA9TXBmkaqdatElia5ti81sJFyAc4IfnzHoag93lMjFotpTae2D0+HUGgiRUN1GXAjbJ2rlfWprRrOcYcJHg+xPG0+PUYTeRFXIMA5t8nOrqXRha6XqDPFbaVcD3m8wPdlcy9F+D//EACsRAAICAQMBCAICAwAAAAAAAAECAxEABBIhMRATICJBUWGBBUIUIzJAcf/aAAgBAgEBPwD/AEJJEiXcx4xvySxvtaFvvrkeojlj3qfrIdVvdlew18LWWKz+UzkiKMkD9jwMQzO3nYCvRR4tfK0U8JqwvNZqJjPIXIq80zAd6Gvbss/WQASwqW+hiKE6ADGB2nbwaNZoe9TWFHuyDfgAs4RtOarRjWRijTDph/F63dWz7vNm0mBPMxPnORJsRUHoM2n2xicRVJDEcgUD4Iltv+DJav5xGIOd5fpkGm2vISOrnnECEsFolevxlqXVBQsgXkyNuIPBBIOAlTQW8VtwvthsvjoSbrAKxeuO5Ln361mgUQ6uaaRxsaPaFyMIZl3GlLC/gZ+WGj8jQkFz/lWd0jNuI5HQ+AXfGLKRw2DaRxjDYl4ESUk/vWSh0U0PN6A4hcqCwo+owOWYjbn4/TRSRM0ig80M10EULju2HPVb6dpBWs6nFAQWxx5C5+MS7oGiRxkUMxlZpiSVPF5NPHCOTyeg9ScUrCpeVgGbk4moHdhgSFYjCS3JN9rMWr4GBiMLFqv07Kqiceytnk4JO41BOpJNcp6jnIIBqLmksgk7VOa1v61RerMKGKKA7KJwqR2jCS5xQR1zVTPDNTKrL+tjO/1TClhrIDvlMkkgLDgDItRFKLU3TbcQAnLA7CgOd2c2HAoA7NVo4dUATww6HNNpmgjKs92ceGM9VU/WJHHGKRQo+BgJGWcs5Z983H3yzm4++WfD/8QALhEAAgEDAgUDAwQDAQAAAAAAAQIDAAQRBRIGEyExQRBRYQcgIhQyUnFAQlNi/9oACAEDAQE/AP8AA0zS73WLtba0j3uQSSThVUd2YnsBVv8ASe5v9PFza61bSP1wI1LISO43Vqmj32j3RtrpNreCOzD3FPDtAI6jHU+nKVcb2x8CmCAdFPXyftyK+m2i2+s8N67CJBHNcssJkAyVXGa4c0SLhnRorITGQRlneQjGS3U19REimsYpwAHF1hD8MDkVKSshAokk9SaiMYlQyAlAw3AdyK1w2tzpUcsAXaGXbjwO2PsdtilsZwKSXmrlRXCXGl3wZfyNt328+BKn9diPkVL9UOHLuEO9+QCM8vYQa4h4ibW5Y2CmO3iyY0Pck/7GmbcxPvSxu/7VJraR3GKLuEMYY7CQSPs1GYxQgDu7AVY8zb/561dxJLHg0bEJ+W/5FQR/qUjfOE2jFR6dJyOcLd+V/wBNp2nHzV1epaI/Kj3bASQPOKjY36JIu0IQCD/dMuGIz2NEY9dTKrbZIz+QxVpcIEChqZ91Tj8CfjpVmsKW6o7sVQYGK0XizQYeE10+dTHOjvvGCRICMA1dM4hmaFQX2sUU+/gVwu+rhZ47oSLEoGwMMYPsKDEDH2SbNuHGQfGM1PYJIBJBhSR28Gm5sTgOCtRNz7hYz2800s+nEAgG33Ht3GahlinAZWyp8imwGODkVgAA5rg/RNOvbGae7ijlJk2rluwFcWaRp+mXSfpJV/L98O7JQ+qvFcBh32tgj2Io4RfAAqeRruXlwoDt7satLQW65bBc9zV50h3FQwVgSD7VzITEvJ27WHcUkbOenbyabMhCoOg7UDNHujV2HkgHocU7vIxZ2LMe5JyfQjIqGBbctjJMjEsaaJWPWo4IoixRQCxyfQyCTcqEHHfzUOyOTYqhVPikto7u2H6dlRx0cOwGRUzmAmJSOnQsKgA3lj2Ao+hdVGSRSTRucKfVhuUj3FIkdvGFGAAKnkRmBU9qgUSRAgkHHXFcuEd5M1J0QIinBq5s57UqJFwWQMP6NTuyRkiirsCeppc+KS6kU/l1FC8TPY0LmMjPWpZmlb48CgCxAAq2up7X8WUlamu1kcYQ1HO+em5almlmbdI7OfcnNMquMEVsXp8VtXGMCuXH/EVyo/4Cti+1cqP+AoIo7Afb/9k=");
                        writer.WritePropertyName("playlistTitle");
                        writer.WriteValue("QueueBot Playlist");
                        writer.WritePropertyName("playlistAuthor");
                        writer.WriteValue("QueueBot");

                        // Creating the Array of Songs
                        writer.WritePropertyName("songs");
                        writer.WriteStartArray();
                        writer.WriteEndArray();
                        writer.WriteEndObject();

                        file.WriteLine(writer.Token);
                    };
            }
        }

        private void checkStoredPlaylist()
        {
            if(songList.Count() > 0)
            {
                foreach(Tuple<string, string> s in songList)
                {
                    bsf.SetUserRoles(true, false, false, s.Item2);
                    bsf.ProcessMessageRecievedAsync("!add " + s.Item1, true);
                }

                songList.Clear();
            }
        }

        private Config ReadCredsFromConfig()
        {
            string channel;
            string token;
            string username;
            bool modonly;
            bool subonly;
            int viewerlimit;
            int sublimit;
            bool continuequeue;
            bool randomize;
            int randomizelimit;
            bool blockMultiRandomQueue;
            bool overrideSongInMultiQueue;
            bool enableShadowQueue;
            bool bsrEnabled;

            if (!Directory.Exists("Config"))
            {
                Directory.CreateDirectory("Config");
            }

            if (!File.Exists("Config\\BeatSaberBotProperties.xml"))
            {
                using (XmlWriter writer = XmlWriter.Create("Config\\BeatSaberBotProperties.xml"))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Config");
                    writer.WriteElementString("Username", "Twitch Username");
                    writer.WriteElementString("Oauth", "Oauth Token");
                    writer.WriteElementString("Channel", "Channel Name");
                    writer.WriteElementString("ModeratorOnly", "false");
                    writer.WriteElementString("SubscriberOnly", "false");
                    writer.WriteElementString("ViewerRequestLimit", "5");
                    writer.WriteElementString("SubscriberLimitOverride", "5");
                    writer.WriteElementString("ContinueQueue", "false");
                    writer.WriteElementString("Randomize", "false");
                    writer.WriteElementString("RandomizeLimit", "5");
                    writer.WriteElementString("BlockUserMultiRandomQueue", "false");
                    writer.WriteElementString("OverrideSongInMultipleQueue", "false");
                    writer.WriteElementString("EnableShadowQueueButton", "false");
                    writer.WriteElementString("BSRCommandEnabled", "true");
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
                return null;
            }

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("Config\\BeatSaberBotProperties.xml");
                XmlNode configNode = doc.SelectSingleNode("Config");

                username = configNode.SelectSingleNode("Username").InnerText.ToLower();
                token = configNode.SelectSingleNode("Oauth").InnerText;
                channel = configNode.SelectSingleNode("Channel").InnerText.ToLower();
                modonly = ConvertToBoolean(configNode.SelectSingleNode("ModeratorOnly").InnerText.ToLower());
                subonly = ConvertToBoolean(configNode.SelectSingleNode("SubscriberOnly").InnerText.ToLower());
                viewerlimit = ConvertToInteger(configNode.SelectSingleNode("ViewerRequestLimit").InnerText.ToLower());
                sublimit = ConvertToInteger(configNode.SelectSingleNode("SubscriberLimitOverride").InnerText.ToLower());
                continuequeue = ConvertToBoolean(configNode.SelectSingleNode("ContinueQueue").InnerText.ToLower());
                randomize = ConvertToBoolean(configNode.SelectSingleNode("Randomize").InnerText.ToLower());
                randomizelimit = ConvertToInteger(configNode.SelectSingleNode("RandomizeLimit").InnerText.ToLower());
                blockMultiRandomQueue = ConvertToBoolean(configNode.SelectSingleNode("BlockUserMultiRandomQueue").InnerText.ToLower());
                overrideSongInMultiQueue = ConvertToBoolean(configNode.SelectSingleNode("OverrideSongInMultipleQueue").InnerText.ToLower());
                enableShadowQueue = ConvertToBoolean(configNode.SelectSingleNode("EnableShadowQueueButton").InnerText.ToLower());

                if (configNode.SelectSingleNode("BSRCommandEnabled") != null)
                {
                    bsrEnabled = ConvertToBoolean(configNode.SelectSingleNode("BSRCommandEnabled").InnerText.ToLower());
                } else
                {
                    XmlNode bsrCommandNode = doc.CreateNode(XmlNodeType.Element, "BSRCommandEnabled", null);
                    bsrCommandNode.InnerText = "true";
                    configNode.InsertAfter(bsrCommandNode, configNode.SelectSingleNode("EnableShadowQueueButton"));
                    doc.Save("Config\\BeatSaberBotProperties.xml");
                    bsrEnabled = ConvertToBoolean(configNode.SelectSingleNode("BSRCommandEnabled").InnerText.ToLower());
                }

                if (String.IsNullOrEmpty(token))
                {
                    logger.Error("OAuth is blank! Need Oauth token to connect to twitch chat");
                    if (MessageBox.Show("OAuth is blank!" + Environment.NewLine + "Please enter Oauth Token and restart the program.", "Error", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                    {
                        Application.Current.Shutdown();
                    }
                } else if(String.IsNullOrEmpty(channel))
                {
                    logger.Error("Channel field blank! Cannot connect to channel.");
                    if (MessageBox.Show("Channel field is blank!" + Environment.NewLine + "Please enter a channel to connect to and restart the program.", "Error", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                    {
                        Application.Current.Shutdown();
                    }
                }

                if (randomize)
                {
                    continuequeue = false;
                }

                return new Config(channel, token, username, modonly, subonly, viewerlimit, sublimit, continuequeue, randomize, randomizelimit, blockMultiRandomQueue, overrideSongInMultiQueue, enableShadowQueue, bsrEnabled);
            }
            catch (Exception e)
            {
                logger.Error("Error loading xml file: " + e);
                return null;
            }
        }

        // Wanted defaults set when conversions failed, so I wrapped in a try/catch block and assigned defaults.
        private bool ConvertToBoolean(string value)
        {
            try
            {
                return Convert.ToBoolean(value);
            }
            catch (FormatException)
            {
                logger.Debug("Value should be a Boolean, but doesn't convert. Default of \"False\" being used.");
                return false;
            }
        }

        private int ConvertToInteger(string value)
        {
            try
            {
                return Int32.Parse(value);
            }
            catch (FormatException)
            {
                logger.Debug("Value should be a Integer, but doesn't convert. Default of \"5\" being used.");
                return 5;
            }
        }

        private void ReadFromBlacklistFile(String filename)
        {
            if (!Directory.Exists("Config"))
            {
                Console.WriteLine("Creating Dir");
                Directory.CreateDirectory("Config");
            }
            if (!File.Exists("Config\\" + filename + ".txt"))
            {
                Console.WriteLine("Creating File");
                using (FileStream fs = File.Create("Config\\" + filename + ".txt"))
                {
                    Console.WriteLine("Created Blacklist File");
                }
            }
            else
            {
                StreamReader file = new StreamReader("Config\\" + filename + ".txt");
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    if (filename.Contains("song_blacklist"))
                    {
                        bsf.AddToBlacklistArrayAsync(line);
                    }
                    else if (filename.Contains("shadow_blacklist"))
                    {
                        bsf.AddToShadowBanArrayAsync(line);
                    }
                }

                file.Close();
            }
        }

        private void btn_move_songs_Click(object sender, RoutedEventArgs e)
        {
            bsf.MoveAllViewerSongsToTop();
            bsf.UpdateListViews();
        }
    }
}
