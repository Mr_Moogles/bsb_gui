﻿using System;
using System.IO;
using System.Xml;

namespace BeatSaberBot
{
    class Config
    {
        public string channel { get; set; }
        public string token { get; set; }
        public string username { get; set; }
        public bool modOnly { get; set; }
        public bool subOnly { get; set; }
        public int viewerLimit { get; set; }
        public int subLimit { get; set; }
        public bool continueQueue { get; set; }
        public bool randomize { get; set; }
        public int randomizeLimit { get; set; }
        public bool blockMultiRandomQueue { get; set; }
        public bool overrideSongInMultiQueue { get; set; }
        public bool enableShadowQueue { get; set; }
        public bool bsrEnabled { get; set; }

        public Config(string channel, string token, string username, bool modOnly, bool subOnly, 
            int viewerLimit, int subLimit, bool continueQueue, bool randomize, int randomizeLimit, bool blockMultiRandomQueue, 
            bool overrideSongInMultiQueue, bool enableShadowQueue, bool bsrEnabled)
        {
            this.channel = channel;
            this.token = token;
            this.username = username;
            this.modOnly = modOnly;
            this.subOnly = subOnly;
            this.viewerLimit = viewerLimit;
            this.subLimit = subLimit;
            this.continueQueue = continueQueue;
            this.randomize = randomize;
            this.randomizeLimit = randomizeLimit;
            this.blockMultiRandomQueue = blockMultiRandomQueue;
            this.overrideSongInMultiQueue = overrideSongInMultiQueue;
            this.enableShadowQueue = enableShadowQueue;
            this.bsrEnabled = bsrEnabled;
        }       
    }
}
