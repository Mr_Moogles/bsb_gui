﻿using System;

namespace BeatSaberBot
{
    public class SongInfo
    {
        public string songName { get; }
        public string beatName { get; }
        public string authName { get; }
        public string uploader { get; }
        public string id { get; }
        public string downloadUrl { get; }
        public string requestedBy { get; }

        public SongInfo(String songname, String beatname, String authname, String uploader, String id, String dlUrl, String requestedBy)
        {
            this.songName = songname;
            this.beatName = beatname;
            this.authName = authname;
            this.uploader = uploader;
            this.id = id;
            this.downloadUrl = dlUrl;
            this.requestedBy = requestedBy;
        }
    }
}
