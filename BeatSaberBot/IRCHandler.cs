﻿using System;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using log4net;

namespace BeatSaberBot
{
    class IRCHandler
    {
        TwitchClient client;
        BeatSaberBotFunc bsf;

        string username;
        string token;
        string channel;
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IRCHandler(Config config)
        {
            username = config.username;
            token = config.token;
            channel = config.channel;

            try
            {
                ConnectionCredentials credentials = new ConnectionCredentials(username, token);

                client = new TwitchClient();
                client.Initialize(credentials, channel);
                bsf = new BeatSaberBotFunc(client, config);

                client.OnJoinedChannel += OnJoinedChannel;
                client.OnMessageReceived += OnMessageReceived;
                client.OnConnected += Client_OnConnected;

                client.Connect();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
        }

        private void Client_OnConnected(object sender, OnConnectedArgs e)
        {
            logger.Info($"Connected to Twitch Channel: {e.AutoJoinChannel}");
        }
        private void OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            logger.Info("Beat Saber Queue Bot Joined!");
        }

        private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            logger.Debug("Processing Twitch Message: " + e.ChatMessage.DisplayName + " -- " + e.ChatMessage.Message);
            bsf.SetUserRoles(e.ChatMessage.IsBroadcaster, e.ChatMessage.IsModerator, e.ChatMessage.IsSubscriber, e.ChatMessage.Username);
            bsf.ProcessMessageRecievedAsync(e.ChatMessage.Message, false);
            logger.Debug("Twitch Message Processed");
        }

        public TwitchClient GetClient()
        {
            return client;
        }

        public BeatSaberBotFunc GetBotFunctions()
        {
            return bsf;
        }
    }
}
