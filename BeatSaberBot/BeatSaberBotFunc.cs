﻿using System;
using System.Collections;
using System.IO;
using System.Threading.Tasks;
using TwitchLib.Client;
using System.Speech.Synthesis;
using log4net;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO.Compression;

namespace BeatSaberBot
{
    class BeatSaberBotFunc
    {
        TwitchClient client;
        Config config;
        MainWindow main;
        DBConnection db = new DBConnection();
        api api;

        private String channel;
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string username = "QueueBot";
        private bool isModerator = false;
        private bool isSubscriber = false;
        private bool isBroadcaster = false;
        private bool isAcceptingRequests = true;

        private ArrayList banList = new ArrayList();
        private ArrayList queueList = new ArrayList();
        private ArrayList randomizedList = new ArrayList();
        private ArrayList userPickedByRandomize = new ArrayList();
        private ArrayList alreadyPlayedList = new ArrayList();
        private ArrayList shadowBanList = new ArrayList(); 

        private bool allowTTS = false;
        private bool allowAudio = false;

        private DateTime blistLastCalled;
        private TimeSpan pauseMin = TimeSpan.FromSeconds(15);
        private bool blistDisable = false;
        private bool downloadStatus = false;
        string downloadPath;

        public BeatSaberBotFunc(TwitchClient client, Config config)
        {
            this.client = client;
            this.config = config;
            channel = config.channel;
            api = new api();
        }

        public void SetUserRoles(Boolean isBroadcaster, Boolean isModerator, Boolean isSubscriber, String username)
        {
            this.username = username;
            this.isBroadcaster = isBroadcaster;
            this.isModerator = isModerator;
                this.isSubscriber = isSubscriber;
            logger.Debug(username + " is Broadcaster: " + isBroadcaster + ", is Moderator: " + isModerator + ", is Subscriber: " + isSubscriber);
        }

        public void ProcessMessageRecievedAsync(String message, Boolean isFromPlaylist)
        {
            if (isModerator || isBroadcaster)
            {
                AdminCommands(message, username, isFromPlaylist);
            }
            else if (config.modOnly)
            {
                if (isModerator || isBroadcaster)
                {
                    BasicCommandsAsync(message, username, isFromPlaylist);
                }
            }
            else if (config.subOnly)
            {
                if (isModerator || isBroadcaster || isSubscriber)
                {
                    BasicCommandsAsync(message, username, isFromPlaylist);
                }
            }
            else if (!config.modOnly && !config.subOnly)
            {
                BasicCommandsAsync(message, username, isFromPlaylist);
            }
        }

        private async void AdminCommands(String command, String requestedBy, Boolean isFromPlaylist)
        {
            string[] parsedCommand = command.Split(new char[0]);
            if ((command.StartsWith("!next", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!clearall", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!remove", StringComparison.OrdinalIgnoreCase) ||
                command.StartsWith("!block", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!close", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!open", StringComparison.OrdinalIgnoreCase) || 
                command.StartsWith("!randomize", StringComparison.OrdinalIgnoreCase)) || command.StartsWith("!sinfo", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!clearusers", StringComparison.OrdinalIgnoreCase) || 
                command.StartsWith("!unblock", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!mtt", StringComparison.OrdinalIgnoreCase))
            {
                if (command.StartsWith("!next", StringComparison.OrdinalIgnoreCase))
                {
                    MoveToNextSongInQueue();
                    UpdateListViews();
                }
                else if (command.StartsWith("!clearall", StringComparison.OrdinalIgnoreCase))
                {
                    ClearAllSongsInPlaylist();
                    RemoveAllSongsFromQueue();
                    UpdateListViews();
                }
                else if (command.StartsWith("!remove", StringComparison.OrdinalIgnoreCase))
                {
                    if (parsedCommand.Length > 1)
                    {
                        SongInfo qs = await api.GetBeatSaverSongInfo(false, parsedCommand[1], requestedBy);
                        if (qs != null)
                        {
                            if (queueList.Count >= 1)
                            {
                                for (int i = 0; i < queueList.Count; i++)
                                {
                                    if (((SongInfo)queueList[i]).songName.Contains(qs.songName))
                                    {
                                        RemoveSongFromJSON(((SongInfo)queueList[i]).id);
                                        queueList.RemoveAt(i);
                                        client.SendMessage(channel, "Removed \"" + qs.songName + "\" from the queue");
                                    }
                                }
                            }
                            else
                                client.SendMessage(channel, "BeatSaber queue was empty.");
                        }
                        else
                            client.SendMessage(channel, "Couldn't Parse Beatsaver Data.");
                    }
                    else
                        client.SendMessage(channel, "Missing Song ID!");

                    UpdateListViews();
                }
                else if (command.StartsWith("!block", StringComparison.OrdinalIgnoreCase))
                {
                    if (parsedCommand.Length > 1)
                    {
                        SongInfo qs = await api.GetBeatSaverSongInfo(false, parsedCommand[1], requestedBy);
                        if (qs != null)
                        {
                            BlacklistSong(qs, false);
                        }
                    }
                    else
                        client.SendMessage(channel, "Missing song ID");

                }
                else if (command.StartsWith("!sban", StringComparison.OrdinalIgnoreCase))
                {
                    if (parsedCommand.Length > 1)
                    {
                        SongInfo qs = await api.GetBeatSaverSongInfo(false, parsedCommand[1], requestedBy);
                        if (qs != null)
                        {
                            BlacklistSong(qs, true);
                        }
                    }
                    else
                        client.SendMessage(channel, "Missing song ID");

                }
                else if (command.StartsWith("!unblock", StringComparison.OrdinalIgnoreCase))
                {
                    if (parsedCommand.Length > 1)
                    {
                        SongInfo qs = await api.GetBeatSaverSongInfo(false, parsedCommand[1], requestedBy);
                        if (qs != null)
                        {
                            if (banList.Count >= 1)
                            {
                                for (int i = 0; i < banList.Count; i++)
                                {
                                    if (((SongInfo)banList[i]).songName.Contains(qs.songName))
                                    {
                                        RemoveBlacklistedSong(qs);
                                        banList.RemoveAt(i);
                                        client.SendMessage(channel, "Removed \"" + qs.songName + "\" from the banlist");
                                    }
                                }
                            }
                            else
                                client.SendMessage(channel, "BeatSaber Banlist was empty.");
                        }
                        else
                            client.SendMessage(channel, "Couldn't Parse Beatsaver Data.");
                    }
                    else
                        client.SendMessage(channel, "Missing Song ID!");

                }
                else if (command.StartsWith("!close", StringComparison.OrdinalIgnoreCase))
                {
                    if (isAcceptingRequests)
                    {
                        isAcceptingRequests = false;
                        client.SendMessage(channel, "The queue has been closed!");
                    }
                    else
                    {
                        client.SendMessage(channel, "The queue was already closed.");
                    }
                }
                else if (command.StartsWith("!open", StringComparison.OrdinalIgnoreCase))
                {
                    if (!isAcceptingRequests)
                    {
                        isAcceptingRequests = true;
                        client.SendMessage(channel, "The queue has been opened!");
                    }
                    else
                    {
                        client.SendMessage(channel, "The queue is already open.");
                    }
                }
                else if (command.StartsWith("!randomize", StringComparison.OrdinalIgnoreCase))
                {
                    Random randomizer = new Random();

                    for (int i = 0;
                     (i < config.randomizeLimit) && (queueList.Count > 0); i++)
                    {
                        int randomIndex = randomizer.Next(queueList.Count);
                        SongInfo randomSong = (SongInfo)queueList[randomIndex];
                        // Check to see if song is shadow banned.
                        bool isShadowBanned = false;
                        foreach (SongInfo banValue in shadowBanList)
                        {
                            if (banValue.songName.Equals(randomSong.songName))
                            {
                                isShadowBanned = true;
                            }
                        }

                        if (!isShadowBanned)
                        {
                            randomizedList.Add(randomSong);
                            downloadSongToDirectory(randomSong, true);
                            queueList.RemoveAt(randomIndex);
                        }
                    }

                    queueList.Clear();
                    queueList.AddRange(randomizedList);
                    randomizedList.Clear();
                    client.SendMessage(channel, config.randomizeLimit + " songs were randomly chosen from queue!");
                    DisplaySongsInQueue(queueList, false);
                    UpdateListViews();

                    if (config.blockMultiRandomQueue)
                    {
                        foreach (SongInfo bsong in queueList)
                        {
                            userPickedByRandomize.Add(bsong.requestedBy);
                        }
                    }
                }
                else if (command.StartsWith("!sinfo", StringComparison.OrdinalIgnoreCase))
                {
                    if (queueList.Count >= 1)
                    {
                        client.SendMessage(channel, "Song Currently Playing: " +
                         "[ID: " + ((SongInfo)queueList[0]).id + "], " +
                         "[Song: " + ((SongInfo)queueList[0]).songName + "], " +
                         "[Download: " + ((SongInfo)queueList[0]).downloadUrl + "]");
                    }
                    else
                        client.SendMessage(channel, "No songs in the queue.");
                }
                else if (command.StartsWith("!clearusers", StringComparison.OrdinalIgnoreCase) && config.blockMultiRandomQueue)
                {
                    userPickedByRandomize.Clear();
                    client.SendMessage(channel, "All users cleared and can request songs.");
                }
                else if (command.StartsWith("!mtt", StringComparison.OrdinalIgnoreCase))
                {
                    if (parsedCommand.Length > 1)
                    {
                        if (queueList.Count >= 1)
                        {
                            MoveSongToTop(parsedCommand[1]);
                            UpdateListViews();
                        }
                        else
                            client.SendMessage(channel, "BeatSaber queue was empty.");
                    }
                    else
                        client.SendMessage(channel, "Missing Song ID or Username!");
                }
            }
            else
                BasicCommandsAsync(command, requestedBy, isFromPlaylist);
        }

        private async void BasicCommandsAsync(String command, String requestedBy, Boolean isFromPlaylist)
        {
            if (command.StartsWith("!bsr", StringComparison.OrdinalIgnoreCase) || command.StartsWith("!add", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    var split = command.Remove(0, 5);
                    if (char.IsDigit(split, 0))
                    {
                        await AddRequestedSongToQueueAsync(false, split, requestedBy, isFromPlaylist);
                    }
                    else
                    {
                        if (config.bsrEnabled)
                        {
                            await AddRequestedSongToQueueAsync(true, command.Remove(0, 5), requestedBy, false);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e);
                }
                UpdateListViews();
            }
            else if (command.StartsWith("!queue", StringComparison.OrdinalIgnoreCase))
            {
                DisplaySongsInQueue(queueList, false);
            }
            else if (command.StartsWith("!blist", StringComparison.OrdinalIgnoreCase))
            {
                if (blistDisable)
                {
                    if(blistLastCalled != null)
                    {
                        DateTime compare = blistLastCalled.Add(pauseMin); ;
                        DateTime currentTme = DateTime.Now;

                        if (currentTme >= compare)
                        {
                            blistDisable = false;
                            DisplaySongsInQueue(banList, true);
                            blistLastCalled = currentTme;
                        }
                    }
                }
                else
                {
                    DisplaySongsInQueue(banList, true);
                    blistLastCalled = DateTime.Now;
                    blistDisable = true;
                }
            }
            else if (command.StartsWith("!qhelp", StringComparison.OrdinalIgnoreCase))
            {
                client.SendMessage(channel, "These are the valid commands for the Beat Saber Queue system.");
                client.SendMessage(channel, "!add <songId>, !queue, !blist, [Mod only] !next, !clearall, !block <songId>, !unblock <songId>, !open, !close, !randomize, !sinfo, !clearusers, !mtt <songId or username>");
            }
        }

        private void BlacklistSong(SongInfo songInfo, bool isShadowBan)
        {
            string blacklistFile = "Config\\song_blacklist.txt";
            if (isShadowBan)
            {
                blacklistFile = "Config\\shadow_blacklist.txt";
            }

            if (songInfo.id != null)
            {
                bool existsInBanList = false;
                foreach(SongInfo song in banList)
                {
                    if (song.id.Equals(songInfo.id))
                    {
                        existsInBanList = true;
                    }
                }

                if (existsInBanList)
                {
                    client.SendMessage(channel, "Song already on banlist");
                }
                else
                {
                    banList.Add(songInfo);
                    if (File.Exists(blacklistFile))
                    {
                        File.AppendAllText(blacklistFile, songInfo.id + Environment.NewLine);
                        if (!isShadowBan)
                        {
                            client.SendMessage(channel, "Added \"" + songInfo.songName + "\", uploaded by " + songInfo.uploader + " to banlist!");
                        }
                    }
                }
            }
        }

        private void RemoveBlacklistedSong(SongInfo songInfo)
        {
            if (songInfo.id != null)
            {
                bool existsInBanList = false;
                foreach (SongInfo song in banList)
                {
                    if (song.id.Equals(songInfo.id))
                    {
                        existsInBanList = true;
                    }
                }

                if (existsInBanList)
                {
                    if (File.Exists("Config\\song_blacklist.txt"))
                    {
                        var tempFile = Path.GetTempFileName();
                        var linesToKeep = File.ReadLines("Config\\song_blacklist.txt").Where(l => l != songInfo.id);

                        File.WriteAllLines(tempFile, linesToKeep);

                        File.Delete("Config\\song_blacklist.txt");
                        File.Move(tempFile, "Config\\song_blacklist.txt");
                    }
                }
            }
        }

        private void MoveToNextSongInQueue()
        {
            if (queueList.Count >= 1)
            {
                alreadyPlayedList.Add(((SongInfo)queueList[0]));
                String remSong = ((SongInfo)queueList[0]).songName;
                RemoveSongFromJSON(((SongInfo)queueList[0]).id);
                queueList.RemoveAt(0);

                if (queueList.Count != 0)
                {
                    if (allowTTS)
                    {
                        speakText("Next song is " + ((SongInfo)queueList[0]).songName + " requested by " + ((SongInfo)queueList[0]).requestedBy);
                    }
                    client.SendMessage(channel, "Removed \"" + remSong + "\" from the queue, next song is \"" + ((SongInfo)queueList[0]).songName + "\" requested by " + ((SongInfo)queueList[0]).requestedBy);
                }
                else
                    client.SendMessage(channel, "Queue is now empty");
            }
            else
                client.SendMessage(channel, "BeatSaber queue was empty.");
        }

        private void DisplaySongsInQueue(ArrayList queue, Boolean isBanlist)
        {
            string curr = "Current song list: ";
            string isEmptyMsg = "No songs in the queue";

            if (isBanlist)
            {
                curr = "Current banned list: ";
                isEmptyMsg = "No songs currently banned.";
            }

            if (queue.Count != 0)
            {
                for (int i = 0; i < queue.Count; i++)
                {
                    if (i < queue.Count - 1)
                    {
                        curr += ((SongInfo)queue[i]).songName + ", ";
                    }
                    else
                        curr += ((SongInfo)queue[i]).songName;
                }

                int charCount = 0;
                int maxLineLength = 400;
                var lines = curr.Split(',').GroupBy(w => (charCount += w.Length + 1) / maxLineLength).Select(g => string.Join(",", g));
                int splitCounter = 0;
                foreach (var line in lines)
                {
                    if (splitCounter > 0)
                    {
                        if (isBanlist)
                        {
                            client.SendMessage(channel, "Banlist cont'd:" + line);
                        } else
                            client.SendMessage(channel, "Queue cont'd:" + line);
                    } else
                        client.SendMessage(channel, line);

                    splitCounter++;
                }
            }
            else
                client.SendMessage(channel, isEmptyMsg);
        }

        private void RemoveAllSongsFromQueue()
        {
            if (queueList.Count != 0)
            {
                queueList.Clear();
                client.SendMessage(channel, "Removed all songs from the BeatSaber queue");
            }
            else
                client.SendMessage(channel, "BeatSaber queue was empty");
        }

        private async Task AddRequestedSongToQueueAsync(Boolean isTextSearch, String queryString, String requestedBy, Boolean isFromPlaylist)
        {
            if (!isAcceptingRequests)
            {
                if (!isModerator && !isBroadcaster)
                {
                    client.SendMessage(channel, "The queue is currently closed.");
                    return;
                }
            }

            string songname = isOSTSong(queryString);
            SongInfo qs;
            if (!String.IsNullOrWhiteSpace(songname))
            {
                string uploader = "Jaroslav Beck";

                if(songname.Contains("Angel Voices"))
                {
                    uploader = "Virtual Self";
                }

                qs = new SongInfo(songname, "", uploader, uploader, queryString, "Included in Beat Saber", requestedBy);
            }
            else
            {
                qs = await api.GetBeatSaverSongInfo(isTextSearch, queryString, requestedBy);
            }

           if (qs != null)
           {
                bool songExistsInQueue = false;
                bool limitReached = false;

                bool isAlreadyBanned = false;
                bool isShadowBanned = false;
                bool userWasAlreadyChosen = false;
                bool songAlreadyPlayed = false;

                // Check to see if song is banned.
                foreach (SongInfo banValue in banList)
                {
                    if (banValue.id.Equals(qs.id))
                    {
                        isAlreadyBanned = true;
                    }
                }

                // Check to see if song has already been played.
                foreach (SongInfo si in alreadyPlayedList)
                {
                    if (!isBroadcaster)
                    {
                        if (!isModerator)
                        {
                            if (si.songName.Equals(qs.songName))
                            {
                                songAlreadyPlayed = true;
                            }
                        }
                    }
                }

                // Check to see if user was selected
                if (config.blockMultiRandomQueue)
                {
                    if (!isBroadcaster)
                    {
                        if (!isModerator)
                        {
                            foreach (String user in userPickedByRandomize)
                            {
                                if (user.Contains(requestedBy))
                                {
                                    userWasAlreadyChosen = true;
                                }
                            }
                        }
                    }
                }

                // Check for song against Blacklist, else proceed to get Song information
                if (isAlreadyBanned)
                {
                    client.SendMessage(channel, "\"" + qs.songName + "\" by " + qs.uploader + " is currently Blacklisted");
                }
                else if (isShadowBanned)
                {
                    // We're currently not going to Display anything to the users
                    // Will write to Console for Debug Purposes
                    logger.Debug("Song is currently Shadow Banned");
                }
                else if (userWasAlreadyChosen && config.blockMultiRandomQueue)
                {
                    client.SendMessage(channel, requestedBy + " you were already chosen for previous queue.");
                }
                else if (songAlreadyPlayed && !config.overrideSongInMultiQueue)
                {
                    client.SendMessage(channel, requestedBy + ", \"" + qs.songName + "\" was chosen for previous queue.");
                }
                else
                {
                    if (queueList.Count != 0)
                    {
                        int internalCounter = 0;

                        foreach (SongInfo q in queueList)
                        {
                            if (q.songName.Equals(qs.songName) || q.id.Equals(qs.id))
                            {
                                songExistsInQueue = true;
                                break;
                            }
                        }

                        foreach (SongInfo q in queueList)
                        {
                            if (q.requestedBy.Equals(requestedBy))
                            {
                                internalCounter++;
                            }
                        }

                        if (!isBroadcaster)
                        {
                            if (!isModerator)
                            {
                                if (config.subLimit == config.viewerLimit)
                                {
                                    limitReached = internalCounter == config.viewerLimit;
                                }
                                else if (!isSubscriber && (internalCounter == config.viewerLimit))
                                {
                                    limitReached = true;
                                }
                                else if (isSubscriber && (internalCounter == config.subLimit))
                                {
                                    limitReached = true;
                                }
                            }
                        }

                        if (!limitReached)
                        {
                            if (!songExistsInQueue)
                            {
                                queueList.Add(qs);
                                downloadSongToDirectory(qs, false);
                                if (!isFromPlaylist)
                                {
                                    client.SendMessage(channel, requestedBy + " added \"" + qs.songName + "\", uploaded by " + qs.uploader + " with id " + qs.id + " to queue!");
                                    AddSongToJSON(qs.songName, qs.requestedBy, qs.id);
                                    //db.runInsert(db.returnQuery(qs.id, qs.songName, qs.uploader, qs.requestedBy, channel));
                                }
                                if (allowAudio)
                                {
                                    speakAudioFile("Config\\AddedSong.wav");
                                }
                            }
                            else
                                client.SendMessage(channel, "\"" + qs.songName + "\" already exists in the queue");
                        }
                        else
                        {
                            client.SendMessage(channel, requestedBy + ", you've reached the request limit.");
                        }
                    }
                    else
                    {
                        queueList.Add(qs);
                        downloadSongToDirectory(qs, false);
                        if (!isFromPlaylist) {
                            client.SendMessage(channel, requestedBy + " added \"" + qs.songName + "\", uploaded by " + qs.uploader + " with id " + qs.id + " to queue!");
                            AddSongToJSON(qs.songName, qs.requestedBy, qs.id);
                            //db.runInsert(db.returnQuery(qs.id, qs.songName, qs.uploader, qs.requestedBy, channel));
                        }
                        if (allowAudio)
                        {
                            speakAudioFile("Config\\AddedSong.wav");
                        }
                    }
                }
            }
            else
                client.SendMessage(channel, "Song Request was Invalid or returned no Results!");
        }

        private void speakText(String text)
        {
            using (SpeechSynthesizer synth = new SpeechSynthesizer())
            {
                synth.SelectVoiceByHints(VoiceGender.Female);
                synth.Speak(text);

            }
        }

        public void speakAudioFile(String wavPath)
        {
            System.Media.SoundPlayer m_SoundPlayer =
             new System.Media.SoundPlayer(wavPath);

            //  Play the output file.
            m_SoundPlayer.Play();
        }

        public void UpdateListViews()
        {
            logger.Debug("Updating List Views");
            System.Windows.Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                main.queueLV.Items.Clear();
                main.songInfoLV.Items.Clear();

                if (queueList.Count >= 1)
                {
                    int counter = 1;
                    foreach (SongInfo song in queueList)
                    {
                        main.queueLV.Items.Add(counter + ": " + song.songName);
                        counter++;
                    }

                    main.songInfoLV.Items.Add("Song ID: " + ((SongInfo)queueList[0]).id);
                    main.songInfoLV.Items.Add("Song Name: " + ((SongInfo)queueList[0]).songName);
                    main.songInfoLV.Items.Add("Song Author: " + ((SongInfo)queueList[0]).authName);
                    main.songInfoLV.Items.Add("Song Uploader: " + ((SongInfo)queueList[0]).uploader);
                    main.songInfoLV.Items.Add("Download URL: " + ((SongInfo)queueList[0]).downloadUrl);
                    main.songInfoLV.Items.Add("Song Requested By: " + ((SongInfo)queueList[0]).requestedBy);
                }
                else
                    main.queueLV.Items.Add("Queue is Empty!");
            }));
        }

        public void RetrieveWindowForm(MainWindow main)
        {
            this.main = main;
        }

        public ArrayList ReturnSongQueue()
        {
            return queueList;
        }

        public Boolean AcceptingRequests()
        {
            return isAcceptingRequests;
        }

        public async void AddToBlacklistArrayAsync(string line)
        {
            SongInfo qs = await api.GetBeatSaverSongInfo(false, line, "");
            if (qs != null)
            {
                banList.Add(qs);
            }
        }

        public async void AddToShadowBanArrayAsync(string line)
        {
            SongInfo qs = await api.GetBeatSaverSongInfo(false, line, "");
            if (qs != null)
            {
                shadowBanList.Add(qs);
            }
        }

        private string isOSTSong(string parsedValue)
        {
            string songName = "";
            switch (parsedValue)
            {
                case "OST1":
                    songName = "$100 Bills";
                    break;
                case "OST2":
                    songName = "Escape ft. Summer Haze";
                    break;
                case "OST3":
                    songName = "Legend ft. Backchat";
                    break;
                case "OST4":
                    songName = "Country Rounds Sqeepo Remix";
                    break;
                case "OST5":
                    songName = "Commercial Pumping";
                    break;
                case "OST6":
                    songName = "Breezer";
                    break;
                case "OST7":
                    songName = "Turn Me On ft. Tiny C";
                    break;
                case "OST8":
                    songName = "Beat Saber";
                    break;
                case "OST9":
                    songName = "Lvl Insane";
                    break;
                case "OST10":
                    songName = "Balearic Pumping";
                    break;
                case "OST11":
                    songName = "Angel Voices";
                    break;
            }

            return songName;
        }

        public void AddSongToJSON(string songName, string requestedBy, string songId)
        {
            if (config.continueQueue)
            {
                var json = File.ReadAllText("Config\\Playlist.json");
                JObject rss = JObject.Parse(json);
                JArray songs = (JArray)rss["songs"];
                JObject song = new JObject(
                    new JProperty("songName", songName),
                    new JProperty("requestedBy", requestedBy),
                    new JProperty("id", songId)
                    );
                songs.Add(song);
                //Console.WriteLine(rss);

                FileInfo fi = new FileInfo("Config\\Playlist.json");
                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write(rss);
                }
            }
        }

        public void RemoveSongFromJSON(string songId)
        {
            if (config.continueQueue)
            {
                var json = File.ReadAllText("Config\\Playlist.json");
                JObject rss = JObject.Parse(json);
                JArray songs = (JArray)rss["songs"];

                for (int i = 0; i < songs.Count(); i++)
                {
                    JToken token = songs[i];
                    string id = (string)token["id"];
                    if (id.Equals(songId))
                    {
                        token.Remove();
                    }
                    else if (id.Contains(songId.Split('-')[0]))
                    {
                        token.Remove();
                    }
                }
                //Console.WriteLine(rss);

                FileInfo fi = new FileInfo("Config\\Playlist.json");
                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write(rss);
                }
            }
        }

        public void ClearAllSongsInPlaylist()
        {
            if (config.continueQueue)
            {
                FileInfo fi = new FileInfo("Config\\Playlist.json");
                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    JTokenWriter writer = new JTokenWriter();
                    writer.WriteStartObject();
                    writer.WritePropertyName("playlistTitle");
                    writer.WriteValue("QueueBot Playlist");
                    writer.WritePropertyName("playlistAuthor");
                    writer.WriteValue("QueueBot");

                    // Creating the Array of Songs
                    writer.WritePropertyName("songs");
                    writer.WriteStartArray();
                    writer.WriteEndArray();
                    writer.WriteEndObject();

                    txtWriter.WriteLine(writer.Token);
                }
            }
        }

        public void setTTSBool(bool allowTTS)
        {
            this.allowTTS = allowTTS;
        }

        public void setAudioBool(bool allowAudio)
        {
            this.allowAudio = allowAudio;
        }

        public void setDownloadValues(bool downloadBool, string gamePath)
        {
            downloadStatus = downloadBool;
            downloadPath = gamePath + "\\CustomSongs";
        }

        public void downloadSongToDirectory(SongInfo song, Boolean allowRandomize)
        {
            if (downloadStatus)
            {
                if (!config.randomize || allowRandomize)
                {
                    if (!Directory.Exists(downloadPath + "\\" + song.id))
                    {
                        Directory.CreateDirectory(downloadPath + "\\" + song.id);
                        using (WebClient client = new WebClient())
                        {
                            // Download. Expect this to be a zip file
                            byte[] data = client.DownloadData(new Uri(song.downloadUrl));
                            MemoryStream memoryStream = new MemoryStream(data);
                            ZipArchive zipArchive = new ZipArchive(memoryStream);

                            foreach (ZipArchiveEntry entry in zipArchive.Entries)
                            {
                                if (entry.FullName.EndsWith("/"))
                                {
                                    zipArchive.ExtractToDirectory(downloadPath + "\\" + song.id + "\\");
                                    break;
                                }
                            }
                        }
                    }
                    else
                        logger.Debug("Skipping \"" + song.songName + "\" for download because song folder already exists");
                }
                else
                {
                    if (config.randomize && !allowRandomize)
                    {
                        logger.Debug("Skipping \"" + song.songName + "\" for download because randomize is enabled. Will download on \"!randomize\" command.");
                    } else
                        logger.Debug("Skipping \"" + song.songName + "\" for download.");
                }
            }
        }

        public void MoveAllViewerSongsToTop()
        {
            ArrayList tempList = new ArrayList();

            int j = 0;
            for(int i = 0; i < queueList.Count; i++)
            {
                var song = queueList[i];
                if (!((SongInfo)song).requestedBy.Equals(config.username, StringComparison.OrdinalIgnoreCase))
                {
                    tempList.Insert(j, song);
                    j++;
                }
                else
                    tempList.Add(song);
            }

            queueList.Clear();
            queueList.AddRange(tempList);
        }

        public void MoveSongToTop(String query)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                var song = queueList[i];
                if (((SongInfo)song).requestedBy.Equals(query, StringComparison.OrdinalIgnoreCase) || 
                    ((SongInfo)song).id.Contains(query))
                {
                    queueList.RemoveAt(i);
                    queueList.Insert(0, song);
                    client.SendMessage(channel, "Moved \"" + ((SongInfo)song).songName + "\" requested by " + ((SongInfo)song).requestedBy + " to top of queue.");
                }
            }
        }

        public String ListShadowBannedSongs()
        {
            string curr = "";
            if (shadowBanList.Count != 0)
            {
                for (int i = 0; i < shadowBanList.Count; i++)
                {
                    if (i < shadowBanList.Count - 1)
                    {
                        curr += ((SongInfo)shadowBanList[i]).songName + ", ";
                    }
                    else
                        curr += ((SongInfo)shadowBanList[i]).songName;
                }

                return curr;

            }
            else
                return "No songs in Shadow Banlist!";
        }
    }
}